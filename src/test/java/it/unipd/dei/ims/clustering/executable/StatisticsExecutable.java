package it.unipd.dei.ims.clustering.executable;

import java.io.IOException;
import java.util.Map;

import it.unipd.dei.ims.statistics.BlazegraphStatisticsWithDatabaseSupport;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/**Executable class to create the statistics and save them into RDB database
 * 
 * */
public class StatisticsExecutable {

	public static void main(String[] args) throws IOException {
		Map<String, String> propertyMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/clustering.properties");

		//get the database path
		String jdbcString = propertyMap.get("jdbc.connectivity.string");
		String databaseFile = propertyMap.get("rdf.database.file");
		boolean labelFlag = Boolean.parseBoolean(propertyMap.get("label.flag"));

		///XXX DEBUG
//		databaseFile = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/rdf_database/dataset.jnl";
		//		jdbcString = "jdbc:postgresql://localhost:5432/DisGeNET_test?user=postgres&password=Ulisse92";

		//compute the statistics
		System.out.println("start computing the statistics");
		BlazegraphStatisticsWithDatabaseSupport stat = new BlazegraphStatisticsWithDatabaseSupport();
		stat.getGraphStatistics(databaseFile, jdbcString, labelFlag);
//		stat.getGraphStatisticsOptimizedFast(jdbcString, jdbcDriver, labelFlag);
		System.out.println("Statistics done");

	}
}
