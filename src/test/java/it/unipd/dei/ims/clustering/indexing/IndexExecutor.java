package it.unipd.dei.ims.clustering.indexing;

import java.util.Map;

import it.unipd.dei.ims.rum.indexing.RumIndexer;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** This class simply uses the information in the properties files to index the cluster files.
 * */
public class IndexExecutor {

	public static void main(String[] args) {
		RumIndexer rumIndexer = new RumIndexer();
		Map<String, String> map = PropertiesUsefulMethods.getProperties();
		
		rumIndexer.setTerrier_home(map.get("terrier.home"));
		rumIndexer.setTerrier_etc(map.get("terrier.etc"));
		
		rumIndexer.setIndexPath(map.get("clusters.index.directory"));
		
		rumIndexer.setMainDirectory(map.get("clusters.turned.to.documents.directory"));
		
		//index
		rumIndexer.rumIndex();
		
	}
}
