package it.unipd.dei.ims.tsa.main;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unid.dei.ims.tsa.online.yosi.TSAYosiAnswerRankingPhase;
import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.tsa.offline.FromRDFGraphsToTRECDocuments;
import it.unipd.dei.ims.tsa.offline.IndexerDirectoryOfTRECFiles;
import it.unipd.dei.ims.tsa.online.AnswerCompressionPhase;
import it.unipd.dei.ims.tsa.online.AnswerRankingPhase;
import it.unipd.dei.ims.tsa.online.blanco.AnswerSubgraphsRankingPhase;
import it.unipd.dei.ims.tsa.online.blanco.RjDocumentCreationPhase;

/** This class executes multiple times the algorithms 
 * of the phase 6 and 7 of the {@link TSAMainClass} class
 * in order to test various parameters.
 * 
 * @author Dennis Dosso
 * */
public class MultipleMergedGraphsExecutioner {

	/** The query we are trying to answer. Hope
	 * God they are not too difficult*/
	private String query;

	/** Id of the query we are managing in the current moment.
	 *  */
	private String queryId;

	/** The main reference directory.
	 * */
	private String mainQueryDirectory;

	private Map<String, String> map;

	public MultipleMergedGraphsExecutioner() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
			this.mainQueryDirectory = map.get("main.query.directory");

			File f = new File(this.mainQueryDirectory);
			if(!f.exists()) {
				f.mkdirs();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Execute the algorithm for each query.
	 * */
	public void executeOnDifferentQueries() {
		try {
			//get the list of query
			String propertiesPath = this.map.get("query.list.file");
			Map<String, String> queryMap = PropertiesUsefulMethods.getSinglePropertyFileMap(propertiesPath);

			System.out.println("\n\n beginning multiple executions "
					+ "of the TSA algorithm to find best parameters\n\n");
			
			Stopwatch timer = Stopwatch.createUnstarted();
			long start = System.nanoTime();
			
			//setup the connection
			ConnectionHandler.createConnectionAsOwner(map.get("jdbc.connection.string"), this.getClass().getName());
			for(Entry<String, String> entry : queryMap.entrySet()) {
				timer.start();
				
				//id of the query
				String queryId = entry.getKey();
				String query = entry.getValue();

				//prepare the execution
				this.setQueryId(queryId);
				this.setQuery(query);
				
				System.out.print("executing for query number " + queryId);

				this.executeForOneQueryInGridOfPoints();

				System.out.println("query ended in " + timer.stop());
				timer.reset();
			}
			//close everything
			long finish = System.nanoTime() - start;
			System.out.print("all computations required " + finish + " nanoseconds");
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
		
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/** Execute one query various times, one for every possible combination of
	 * points.
	 * */
	private void executeForOneQueryInGridOfPoints() {
		//first phase, first rank (only once)
		try {
			this.firstIrRank_1();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(int lookahead = 5; lookahead <= 30; lookahead+=5) {
			//changing on the lookahead with step 5 from 5 to 30
			for(double overlapping = 0.1; overlapping <= 0.9; overlapping += 0.1) {
				System.out.println("Executing for overlapping " + overlapping + " and "
						+ "lookahead " + lookahead + " for query number " + queryId);
				//iteration over the overlapping parameter with step .1 from 0.1 to 0.9
				try {
					this.merging_2(overlapping, lookahead);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				this.indexing_3(overlapping, lookahead);
				try {
					this.rankWithIR_4(overlapping, lookahead);
//					this.rankWithBlanco_5(overlapping, lookahead);
//					this.rankWithYosi_6(overlapping, lookahead);
				} catch (IOException e) {
					System.err.println("lookahead:" + lookahead + ", overlapping " + overlapping +
							", query: " + this.query + ", queryId: " + queryId);
					e.printStackTrace();
				}
			}
		}
		
		
	}

	/** First execution: creation of a run with simple IR BM25
	 * on the whole cluster collection. Limit the answers to 10.000.
	 * */
	private void firstIrRank_1 () throws IOException {
		//create the object that rank with BM25 
		AnswerRankingPhase phase6 = new AnswerRankingPhase();

		phase6.setIndexPath(map.get("index.path"));
		phase6.setQuery(query);
		phase6.setResultDirectory(mainQueryDirectory + "/" + queryId);
		
		File f = new File(mainQueryDirectory + "/" + queryId);
		if(!f.exists())
			f.mkdirs();
		
		phase6.setQueryId(queryId);
		phase6.setAnswerFileName("firstRun.txt");

		phase6.subgraphsRankingPhase();
	}

	/** Second phase: use the first run file to create a new collection
	 * of graphs merged with different parameters.
	 * 
	 * @param overlapping the percentage of overlapping considered in this run.
	 * @param lookahead number of the window of graphs used 
	 * @throws IOException 
	 * */
	private void merging_2(double overlapping, int lookahead) throws IOException {

		//create the merger
		AnswerCompressionPhase phase = new AnswerCompressionPhase();
		
		phase.setCompressionThreshold(overlapping * 100);
		phase.setLookaheadThreshold(lookahead);
		
		//get the run file of the previous phase
		phase.setRankingFilePath(mainQueryDirectory + "/" + queryId + "/firstRun.txt");
		//get the directory of the graphs referred by the run file
		phase.setOriginalCollectionDirectoryPath(map.get("cluster.directory.path")); 
		//directory where to save the collection
		phase.setCompressedCollectionOutputDirectoryPath(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/graphs" );

		
		//generate the collection
		phase.compressTheCollectionWithThreshold();
	}

	/** Index the final collection.
	 * */
	private void indexing_3( double overlapping, int lookahead ) {
		//converter from graphs to TREC
		FromRDFGraphsToTRECDocuments phase7bis = new FromRDFGraphsToTRECDocuments();

		phase7bis.setGraphsMainDirectory(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/graphs"); 
		phase7bis.setOutputDirectory(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/collection");
		phase7bis.convertRDFGraphsInTRECDocuments();

		//index them
		IndexerDirectoryOfTRECFiles phase72bis = new IndexerDirectoryOfTRECFiles();

		phase72bis.setDirectoryToIndex(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/collection");
		phase72bis.setIndexPath(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/unigram_index");

		phase72bis.index("unigram");

		phase72bis.setIndexPath(mainQueryDirectory + "/" + queryId + "/collections" + "/collection_" + overlapping + "_" + lookahead + "/bigram_index");
		phase72bis.index("bigram");
	}

	private void rankWithIR_4 ( double overlapping, int lookahead ) throws IOException {
		AnswerRankingPhase phase6 = new AnswerRankingPhase();

		phase6.setQuery(query);
		phase6.setAnswerFileName("run.txt");
		phase6.setIndexPath(mainQueryDirectory + "/" + queryId + "/collections" + "/collection_" + overlapping + "_" + lookahead + "/unigram_index");
		phase6.setResultDirectory(mainQueryDirectory + "/" + queryId + "/collections" + "/collection_" + overlapping + "_" + lookahead + "/IR/run");
		phase6.setQueryId(queryId);

		phase6.subgraphsRankingPhase();
	}

	private void rankWithBlanco_5 ( double overlapping, int lookahead ) throws IOException {
		//creation of rj documents and indexes
		RjDocumentCreationPhase phase8 = new RjDocumentCreationPhase();

		phase8.setAnswerSubgraphsDirectory(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/graphs");
		phase8.setRjOutputDirectory(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/BLANCO/rj");

		phase8.createTheRjFiles();

		//rank with BLANCO
		AnswerSubgraphsRankingPhase phase9 = new AnswerSubgraphsRankingPhase();
		phase9.setQuery(query);
		phase9.setQueryId(this.queryId);
		//set where to save the result
		phase9.setResultDirectory(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/BLANCO/rank");

		phase9.setIndexPath(mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/unigram_index"); 

		//set properties for the Blanco-Elbassuoni Language Model 
		System.setProperty("blanco.rj.indexes.directory", mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/BLANCO/rj_indexes");
		System.setProperty("blanco.rj.index.directory", mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/BLANCO/rj_index");
		System.setProperty("blanco.r.j.output.directory", mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/BLANCO/rj");
		System.setProperty("answer.index.directory", mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/unigram_index");
		System.setProperty("blanco.subgraphs.directory", mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/graphs");
		
		phase9.subgraphsRankingPhase();
	}

	private void rankWithYosi_6 ( double overlapping, int lookahead  ) throws IOException {
		//need to create a bigram index of the candidate answers
		System.out.println("Start phase 6: rank with Yosi");

		TSAYosiAnswerRankingPhase phase10 = new TSAYosiAnswerRankingPhase();

		phase10.setQuery(query);
		phase10.setQueryId(this.queryId);
		phase10.setUnigramIndexPath(mainQueryDirectory + "/" + queryId + "/collections" + "/collection_" + overlapping + "_" + lookahead + "/unigram_index");
		phase10.setBigramIndexPath(mainQueryDirectory + "/" + queryId + "/collections" + "/collection_" + overlapping + "_" + lookahead + "/bigram_index");
		phase10.setAnswerRankOutputFilePath(this.mainQueryDirectory + "/" + queryId + "/collections/collection_" + overlapping + "_" + lookahead + "/YOSI/rank/run.txt");

		phase10.initialize();
		phase10.rankTheAnswers();

	}

	/** Test main */
	public static void main(String[] args) {
		MultipleMergedGraphsExecutioner execution = new MultipleMergedGraphsExecutioner();
		execution.executeOnDifferentQueries();
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

}
