package it.unipd.dei.ims.tsa.online;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.tsa.datastructure.VNode;
import it.unipd.dei.ims.yosi.datastructures.YosiDocument;
import it.unipd.dei.ims.yosi.datastructures.YosiDocumentComparator;

/** Phase 11 of the TSA algorithm revisited (phase 11 bis).
 * <p>
 * Here we create the graphs of the virtual docs online.
 * Thus is made in order to see if we can bee even more quick.
 * */
public class VirtualDocAndPruningOnlinePhase {

	/** The radius we are allowing.*/
	protected int tau;

	/** Directory where to write the answer graphs 
	 * before we prune them.
	 * */
	protected String beforePruningAnswerGraphsDirectory;

	/** Directory where to read the graphs 
	 * that are going to create the base of the 
	 * query graph*/
	protected String readingGraphsDirectory;

	/** Path of the run file with the first rank
	 * */
	protected String runFile;

	protected Map<String, String> map;

	/** path for the result file with the ranking. 
	 * */
	protected String resultOutputFile;

	/** map with the IRI of a graph with its root IRI
	 * */
	protected Map<String, String> pathMap;

	/** The limit of the number of answer graphs we are going to read
	 * from the first rank file.
	 * */
	protected int limit;

	protected Connection connection;

	protected String jdbcConnectionString;

	/** Where to save the answer graphs.
	 * */
	protected String answerGraphOutputDirectory;

	protected String unigramIndexPath;
	protected String bigramIndexPath;
	protected Index unigramClusterIndex;
	protected Index bigramClusterIndex;


	/** the query we are dealing with*/
	protected String query;

	protected String queryId;

	/** counter for the answer graphs that we are printing*/
	protected int answerGraphCounter;

	/** List were we are going to store the answers with their score*/
	List<YosiDocument> docList;

	protected int answerCounter;
	
	protected String schema = "public";

	protected static String SQL_GET_NEIGHBOURS = "select subject_, predicate_, object_ from triple_store where subject_ = ?";

	public VirtualDocAndPruningOnlinePhase() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			tau = Integer.parseInt(map.get("virtual.graphs.tau"));
			this.beforePruningAnswerGraphsDirectory = map.get("before.pruning.answer.graphs.directory");
			this.runFile = map.get("run.file");
			this.limit = Integer.parseInt(map.get("smart.traversal.limit"));

			this.jdbcConnectionString = map.get("jdbc.connection.string");

			pathMap = new HashMap<String, String>();

			docList = new ArrayList<YosiDocument>();

			String terrierHome = map.get("terrier.home");
			String terrierEtc = map.get("terrier.etc");

			System.setProperty("terrier.home", terrierHome);
			System.setProperty("terrier.etc", terrierEtc);

			unigramIndexPath=map.get("test.unigram.index.path");
			this.bigramIndexPath=map.get("test.bigram.index.path");

			query=map.get("test.query");

			this.answerGraphOutputDirectory=map.get("test.answer.graph.output.directory");
			this.readingGraphsDirectory = map.get("test.answer.graphs.directory");

			resultOutputFile = map.get("test.result.output.file");
			
			this.schema = map.get("schema");
			
			SQL_GET_NEIGHBOURS = "select subject_, predicate_, object_ "
					+ "from " + this.schema + ".triple_store where subject_ = ?";


		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/** Principal method of the class. Creates the answer graphs and ranks them,
	 * then prints the answers.
	 * */
	public void smartTraversing() throws IllegalArgumentException {
		Model g = null;
		if(DatabaseState.getSize() == DatabaseState.SMALL) {
			//better for smaller graphs
			System.out.println("Settings for a small dataset in VDP");
			g = this.readQueryGraphNoFilterVersion();			
		}
		if(DatabaseState.getSize() == DatabaseState.BIG) {
			//better for bigger graphs
			System.out.println("Settings for a big dataset in VDP");
			try {
				g = this.readQueryGraph();							
			} catch (java.nio.file.NoSuchFileException e) {
				System.err.println("no files to read");
				return;
			}
		}
		
		try {
			
			this.findAnswerGraphs(g);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			throw e;
		}
		//now we rank the answers
		this.scoreTheAnswers();
	}

	/** Reads graphs from the run file obtained from the
	 * standard BM25 method, then keeps only the ones
	 * containing all the query words and uses them to build the final query graph.
	 * */
	public Model readQueryGraph() throws NoSuchFileException {
		//read the lines of the run file in order to get the id of the graphs
		Path path = Paths.get(this.runFile);
		try(BufferedReader reader = Files.newBufferedReader(path)) {
			String line = "";

			int counter = 0;
			Model graph = new TreeModel();
			
			List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
			loggers.add(LogManager.getRootLogger());
			for ( Logger logger : loggers ) {
			    logger.setLevel(Level.OFF);
			}
			
			
			//read one graph
			while((line = reader.readLine()) != null && counter < limit) {
				
				if(Thread.interrupted() || !ThreadState.isOnLine()) {
					ThreadState.setOnLine(false);
					break;
				}
				
				counter++;
				//take the line and get the id of the graph, third component
				String[] parts = line.split(" ");
				String id = parts[2];
				//rebuild the path of the graph
				String graphPath = this.readingGraphsDirectory + "/" + id + ".ttl";
				Model mol = new TreeModel(BlazegraphUsefulMethods.readOneGraph(graphPath));
				//transform the graph in document
				String graphDocument = BlazegraphUsefulMethods.fromGraphToDocument(mol);
				//index it and check the query Words
				System.setProperty("tokeniser", "EnglishTokeniser");
				List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
				MemoryIndex memIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(graphDocument);
				//get the lexicon
				Lexicon<String> lex = memIndex.getLexicon();
				boolean returning = true;
				
				for(String queryWord : queryWords) {
					LexiconEntry le = lex.getLexiconEntry(queryWord);
					if(le == null) {
						returning = false;
						break;
					}
				}
				
				if(returning) {
					//the graph can be added to the query graph
					graph.addAll(mol);
				}
				
				memIndex.close();
			}
			
			System.err.println("The dimension of this query graph is : " + graph.size());
			return graph;
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.err.println("no query graph produced");
		return null;
	}




	/** Read the ids of the first graphs from the ultimate run obtained from 
	 * a run  
	 * and use them to build up a graph which is the union of all the other graphs.
	 * THis big graph is our own query graph.
	 * <p>
	 * In this first version of the algorithm, the read graphs were not filtered by 
	 * the presence of all the query words. i.e. we take all the graphs
	 * we read from the run. When for some reasons these
	 * graphs tend to be big, one heuristic can be the one
	 * to take only the graphs that contain all the
	 * query words in them.
	 * 
	 * 
	 * @return a Model with the triples of the query graph
	 * */
	public Model readQueryGraphNoFilterVersion() {

		//read the lines of the run file in order to get the id of the graphs
		Path path = Paths.get(this.runFile);
		try(BufferedReader reader = Files.newBufferedReader(path)) {
			String line = "";

			int counter = 0;
			Model graph = new TreeModel();
			int oldSize = 0;

			while((line = reader.readLine()) != null && counter < limit) {
				counter++;
				//take the line and get the id of the graph, third component
				String[] parts = line.split(" ");
				String id = parts[2];
				//rebuild the path of the graph
				String graphPath = this.readingGraphsDirectory + "/" + id + ".ttl";
				Collection<Statement> col = BlazegraphUsefulMethods.readOneGraph(graphPath);
				graph.addAll(col);
			}

			//DEBUG print
			System.out.println("The dimension of this query graph is : " + graph.size());
			return graph;

		} 
//		catch (NoSuchFileException e) {
//			throw new IllegalStateException("No file for query " + queryId);
//		} 
		catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
		return null;
	}


	/** Explores the whole query graph looking for good subgraphs, with all the query words.
	 * Saves these graphs in a temporary directory. 
	 * This directory contains answer graphs that still
	 * have to be pruned.
	 * <p>
	 * Only the virtual graphs with all the query words are kept.
	 * 
	 * 
	 * @throws SQLException 
	 * */
	public void findAnswerGraphs(Model graph) throws SQLException, IllegalArgumentException {
		if(graph == null)
			throw new IllegalArgumentException("no documents in query " + queryId);
		
		connection = ConnectionHandler.createConnectionAsOwner(this.jdbcConnectionString, this.getClass().getName());
		
		//create/clean the output directory
		File f = new File(this.beforePruningAnswerGraphsDirectory);
		if(!f.exists()) {
			f.mkdirs();
		}
		try {
			FileUtils.cleanDirectory(f);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		//set to keep track of the subjects already checked
		Set<String> alreadyExploredSubjects = new HashSet<String>();
		//words of the query
		System.setProperty("tokeniser", "EnglishTokeniser");//always set the right tokeniser
		//this is useful when dealing with multiple queries one after the other
		List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
		int progressCounter = 0;
		int graphSize = graph.size();

		//now take the graph and list all the subjects
		for(Statement t : graph) {
			
			if(Thread.interrupted() || !ThreadState.isOnLine()) {
				ThreadState.setOnLine(false);
				return;
			}
				
			String subject = t.getSubject().toString();
			if(!alreadyExploredSubjects.contains(subject)) {
				alreadyExploredSubjects.add(subject);
				//explore the surroundings of this graph
				this.dealWithOneRoot(subject, graph, queryWords);
			} 
			progressCounter++;
			if(progressCounter%1000==0) {
				//debug print
				System.out.println("\nchecked " + progressCounter + " graphs out of " + graphSize);
			}
		}

	}

	/** Creates the surroundings of a node, creates the graphs and checks that 
	 * it can be used as root graph. If so, it print it.
	 * <p>
	 * NB: it is necessary to create the virtual graph from the 
	 * whole graph, not only the query graph. This is necessary in order
	 * to obtain grater connectivity 
	 * and improve the final results. Trust me, I am an engineer
	 * (and I saw some performances).
	 * The consequence is that this method 
	 * uses the relational database.
	 * 
	 * 
	 * @throws SQLException 
	 * */
	protected void dealWithOneRoot(String subject, Model graph, List<String> queryWords) throws SQLException {
		Model subgraph = new TreeModel();

		//perform a BFS algorithm
		VNode source = new VNode();
		source.setIri(subject);
		source.setRadius(0);

		//a queue that contains IRIs to visit
		Queue<VNode> iris = new LinkedList<VNode>();
		iris.add(source);

		while(!iris.isEmpty()) {
			VNode v = iris.remove();
			String vIri = v.getIri();
			int vRadius = v.getRadius();

			//find the neighbors
			PreparedStatement ps = connection.prepareStatement(SQL_GET_NEIGHBOURS);
			ps.setString(1, vIri);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				String subj = rs.getString("subject_");
				String pred = rs.getString("predicate_");
				String obj = rs.getString("object_");

				Statement triple = BlazegraphUsefulMethods.createAStatement(subj, pred, obj);

				//add the triple to the subgraph
				subgraph.add(triple);

				//if we reached the limit of the radius, stop
				if(vRadius + 1 > this.tau)
					continue;

				//check the object
				String object = triple.getObject().stringValue();
				//add it to the queue if it is a IRI
				if(UrlUtilities.checkIfValidURL(object)) {
					VNode u = new VNode();
					u.setIri(object);
					u.setRadius(vRadius + 1);
					iris.add(u);
				}
			}
		}//end while
		//now we have the whole graph

		//extrapolate the text from the graph
		String graphDocument = BlazegraphUsefulMethods.fromGraphToDocument(subgraph);
		//index this graph
		try {
			System.setProperty("tokeniser", "EnglishTokeniser");
			MemoryIndex memIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(graphDocument);
			//check if all the query words are present in the document

			//get the lexicon
			Lexicon<String> lex = memIndex.getLexicon();
			//boolean to help us understand if this graph is fit to be returned
			boolean returning = true;

			for(String queryWord : queryWords) {
				LexiconEntry le = lex.getLexiconEntry(queryWord);
				if(le == null) {
					returning = false;
					break;
				}
			}

			if(returning) {
				//this is an answer graph (contains all the query words), and we can print it
				this.answerGraphCounter++;
				String outPath = this.beforePruningAnswerGraphsDirectory + "/" + this.answerGraphCounter + ".ttl";
				BlazegraphUsefulMethods.printTheDamnGraph(subgraph, outPath);
				PreparedStatement ps = connection.prepareStatement("SELECT id_ from " + this.schema + ".node where node_name = ?");
				ps.setString(1, subject);
				ResultSet rs = ps.executeQuery();
				int docno = -1;
				if(rs.next()) {
					docno = rs.getInt("id_");
				} else {
					System.err.print("strange subject: " + subject);
				}
				
				//Debug
				System.out.print(docno + ", ");

				pathMap.put(subject, outPath);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** Scores all the documents inside the before pruning answer directory.
	 * 
	 * So it prunes the answers, obtaining the new answers graphs, 
	 * and ranks them producing the output file.
	 *  
	 * */
	protected void scoreTheAnswers() {
		this.unigramClusterIndex = IndexOnDisk.createIndex(this.unigramIndexPath, "data");
		this.bigramClusterIndex = IndexOnDisk.createIndex(this.bigramIndexPath, "data");

		File f = new File(this.answerGraphOutputDirectory);
		if(!f.exists()) {
			f.mkdirs();
		}
		try {
			FileUtils.cleanDirectory(f);
		} catch (IOException e1) {
			e1.printStackTrace();
		}


		//read all the files inside the answer directory
		//we have the path inside the this.pathMap field,
		//prepared in the previously invoked method
		for(Entry<String, String> entry : pathMap.entrySet()) {
			//get the root iri and the path of the graph to rank
			String rootIri = entry.getKey();
			String path = entry.getValue();

			// scores one graph and puts the data in this.docList so we can later rank
			this.scoreOneGraph(rootIri, path);
		}
		//order the rank
		Collections.sort(docList, new YosiDocumentComparator());
		//print the answer file
		File f1 = new File(this.resultOutputFile).getParentFile();
		if(!f1.exists()) {
			f1.mkdirs();
		}

		Path outptPath = Paths.get(this.resultOutputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outptPath, UsefulConstants.CHARSET_ENCODING); ) {
			for(int i = 0; i < docList.size(); ++i) {
				YosiDocument doc = docList.get(i);
				writer.write("query_no " + this.queryId + " " + doc.getDocno() + " " + i + " " + doc.getScore() + " TSA+VDP \n" );
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/** Reads a graph from the provided path.
	 * 
	 * @param rootIri the iri of the root node of this graph
	 * @param graphPath the path of the graph in the file system
	 * */
	protected void scoreOneGraph(String rootIri, String graphPath) {
		//get the id of the graph so we know what the hell we are ranking
		String graphId = (new File(graphPath)).getName();
		String[] parts = graphId.split("\\.");
		graphId = parts[0];

		//read the graph from the file turtle and set 
		//the objects in order to be able to easily prune it later
		try {
			Model graph = new TreeModel(BlazegraphUsefulMethods.readOneGraph(graphPath));

			//now execute the BFS
			//queue for the discovered nodes
			Queue<VNode> nodeQueue = new LinkedList<VNode>();
			//list for the IRI already discovered
			List<String> alreadyVisitedIRI = new ArrayList<String>();

			//root (starting) node
			VNode rootNode = new VNode();
			//set the iri of the root and add to the queue and to the already discovered nodes
			rootNode.setIri(rootIri);
			rootNode.setId(Integer.parseInt(graphId));
			nodeQueue.add(rootNode);
			alreadyVisitedIRI.add(rootIri);

			while(! nodeQueue.isEmpty() ) {
				VNode s = nodeQueue.poll();
				//now find the neighbors of the node
				Resource subject = new URIImpl(s.getIri());
				Model outStar = graph.filter(subject, null, null);

				//now outStar contains all the triples with s as subject (if any)
				for(Statement t : outStar) {
					//take the object and check if it is a literal or an IRI
					String object = t.getObject().toString();
					if(!alreadyVisitedIRI.contains(object)) {
						//a new object


						if(UrlUtilities.checkIfValidURL(object)) {

							//sign that it has been discovered or we are all in trouble
							alreadyVisitedIRI.add(object);

							//if it is a IRI, we need to create a new node
							VNode objectNode = new VNode();
							objectNode.setIri(object);
							objectNode.setPrevious(s);
							objectNode.setPreviousStatement(t);

							//set this node as a neighbour for s
							s.addNxt(t, objectNode);

							nodeQueue.add(objectNode);
						} else {
							//this is simply a literal object, and we add it in the accessory cloud of s
							s.addTriple(t);
						}
					} else {
						/*this object node has already been explored
						 * the triple can tell something new thanks to the predicate,
						 * so for now I add it in the accessory cloud*/
						s.addTriple(t);
					}
				}
			}//end while

			//now we found ourselves with a chain of nodes. We can prune them using the query words

			System.setProperty("tokeniser", "EnglishTokeniser");
			List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
			System.setProperty("tokeniser", "BigramTokeniser");
			List<String> queryWordsBigrams = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
			System.setProperty("tokeniser", "EnglishTokeniser");
			this.pruneFromRootWithQueryWords(rootNode, queryWords);

			//now we have all nice and pruned, so we proceed to create the graph 
			//with Blazegraph Model object and to score it
			try {
				this.buildAndScoreTheGraph(rootNode, queryWords, queryWordsBigrams, graphId);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
	}

	/** pruning heuristic starting from the root
	 * */
	protected void pruneFromRootWithQueryWords(VNode root, List<String> queryWords) {
		Queue<VNode> executingQueue = new LinkedList<VNode>();
		executingQueue.add(root);
		List<VNode> leaves = new ArrayList<VNode>();
		int counter = 0;

		while(! executingQueue.isEmpty() ) {
			//take the node
			VNode s = executingQueue.poll();
			//first of all, look at all the accessory triples around the root
			List<Statement> cloud = s.getNodeCloud();
			//get an iterator over the cloud
			Iterator<Statement> iterator = cloud.iterator();

			//prune the cloud
			while(iterator.hasNext()) {
				//get the statement
				Statement t = iterator.next();
				/*check only predicate and object, otherwise it is useless
				 * all triples will be maintained*/
				String pred = t.getPredicate().toString();
				String obj = t.getObject().stringValue();
				String tripleDoc = "";
				tripleDoc = UrlUtilities.takeWordsFromIri(pred);
				if(UrlUtilities.checkIfValidURL(obj)) {
					tripleDoc += " " + UrlUtilities.takeWordsFromIri(obj);
				} else {
					tripleDoc += " " + obj;
				}
				List<String> tripleWords = 
						TerrierUsefulMethods.
						getDocumentWordsWithTerrierAsList(tripleDoc);
				tripleWords.retainAll(queryWords);
				if(tripleWords.size() == 0){//if the triple has no query words, it deserves to be deleted 
					//need to delete the triple
					iterator.remove();
					counter++;
				}

			}//end pruning the cloud phase

			//now look at the neighbors
			List<Pair<Statement, VNode>> neighborsList = s.getNxtsList();
			for(Pair<Statement, VNode> pair : neighborsList) {
				VNode u = pair.getRight();
				executingQueue.add(u);
			}

			if(neighborsList.size() == 0) {
				leaves.add(s);
			}

		}//end of the first direct traversal

		//now pass the ball to the pruning from the leaves
		this.pruneFromTheLeavesWithQueryWords(leaves, queryWords);
	}


	/** Here we prune from the leaves going back to the root */
	protected void pruneFromTheLeavesWithQueryWords(List<VNode> leaves, List<String> queryWords) {
		
		Model prunedGraph = new TreeModel();
		
		//first of all, create a queue with the nodes we are visiting starting from the leaves
		Queue<VNode> executingQueue = new LinkedList<VNode>(leaves);
		int count = 0;
		while(!executingQueue.isEmpty()) {
			VNode s = executingQueue.poll();
			if(s.isToSpare())
				continue;

			//check if this node and its surroundings deserved to be maintained or not

			//string representing the document connected to this node
			String nodeDoc = "";

			/*tecnically if I am here with some literal
			 * neighbours the node should be maintained because this nodes keep
			 * some query words inside of them. But better be safe*/

			//take the accessory cloud of this node
			List<Statement> cloud = s.getNodeCloud();
			for(Statement t : cloud) {
				//take the text from the surrounding triples
				String pred = t.getPredicate().toString();
				String obj = t.getObject().stringValue();
				nodeDoc += " " + UrlUtilities.takeWordsFromIri(pred);
				if(UrlUtilities.checkIfValidURL(obj)) {
					nodeDoc += " " + UrlUtilities.takeWordsFromIri(obj);
				} else {
					nodeDoc += " " + obj;
				}
			}//end for on the cloud

			//add the contribution of this node
			String iri = s.getIri();
			nodeDoc += " " + UrlUtilities.takeWordsFromIri(iri);

			//add the contribution from the connecting triple if present
			Statement t = s.getPreviousStatement();
			String pred = "";
			if(t!=null) {//for the root
				pred = t.getPredicate().toString();
				nodeDoc += " " + UrlUtilities.takeWordsFromIri(pred);
			}

			//now convert the document in a list of strings
			List<String> nodeWords = 
					TerrierUsefulMethods.
					getDocumentWordsWithTerrierAsList(nodeDoc);
			nodeWords.retainAll(queryWords);

			//now, if the list is empty, remove this node from the path
			if(nodeWords.size() == 0) {
				VNode parent = s.getPrevious();

				if(parent==null)
					continue;

				List<Pair<Statement, VNode>> list = parent.getNxtsList();
				Iterator<Pair<Statement, VNode>> iterator =  list.iterator();
				while(iterator.hasNext()) {
					Pair<Statement, VNode> pair = iterator.next();
					Statement st = pair.getLeft();
					VNode neighbor = pair.getRight();
					if(st.getPredicate().toString().equals(pred) &&
							neighbor.isEqualByIri(s)) {
						iterator.remove();
						break;//there is only one!
					}
				} //end of remotion

				//now, we need to add the parent to the list of nodes to be checked
				if(!parent.isToSpare())
					executingQueue.add(parent);
			} else {
				/*we need to signal to the parent that one of its neighbor
				 * is ok, in order to avoid to break paths that don't 
				 * need to be broken*/
				VNode parent = s.getPrevious();
				if(parent!=null)//it could be the root
					parent.setToSpare(true);
			}

		}
		//now the pruning is complete
	}

	/** Builds, prints and scores the graph starting
	 * with the provided root.
	 * 
	 * @param root a VNode contining the information of the root node,
	 * comprising the links to the next nodes in the graph.
	 *  
	 * @throws Exception */
	protected void buildAndScoreTheGraph(VNode root, List<String> queryWords,
			List<String> queryWordsBigram,
			String graphId) throws Exception {
		Model graph = new TreeModel();
		Queue<VNode> nodeQueue = new LinkedList<VNode>();
		nodeQueue.add(root);

		//map with the wtf for every word inside this document
		Map<String, Double> unigramMap = new HashMap<String, Double>();
		Map<String, Double> bigramMap = new HashMap<String, Double>();
		double rootStaticScore = 0;

		//begin BFS
		while( !nodeQueue.isEmpty() ) {
			VNode s = nodeQueue.poll();
			//get the triples of the cloud
			List<Statement> cloud = s.getNodeCloud();
			//add them to the graph
			graph.addAll(cloud);
			//now take all the neighbors
			List<Pair<Statement, VNode>> neighborsList = s.getNxtsList();

			//now for the static score of the path
			//get the static weight of the path so far
			double staticWeight = s.getStaticPathWeight();
			if(staticWeight == 0) {
				//special case, the root
				staticWeight = (double) 1 / (Math.log(Math.E + neighborsList.size()));
				rootStaticScore = staticWeight;
			} else {
				//add to the static weight computed so far the contribution of the node itself
				staticWeight += (double) 1 / (Math.log(Math.E + neighborsList.size()));
			}
			s.setStaticPathWeight(staticWeight);

			///////
			//update the tables with the information about the weighted frequencies of the words
			this.updateWordsContribution(s, unigramMap, bigramMap, rootStaticScore, staticWeight);

			Iterator<Pair<Statement, VNode>> iter = neighborsList.iterator();
			while(iter.hasNext()) {
				Pair<Statement, VNode> pair = iter.next();
				//get the statement and add it to the graph
				Statement t = pair.getLeft();
				graph.add(t);
				//get the neighbor and add it to the queue
				VNode u = pair.getRight();
				nodeQueue.add(u);
				//set the weight of the path until s, then u will complete the score with itself
				u.setStaticPathWeight(staticWeight);
			}
		}//end of the construction of the graph
		//now we can score and print the graph

		//compute the length for unigrams and bigrams
		Pair<Double, Double> lengths = this.computeTheLengths(unigramMap, bigramMap);

		//now we have all the information to score the graph. And
		//we do exactly that
		double finalScore = this.scoreThisGraph(unigramMap, bigramMap, queryWords, queryWordsBigram, lengths, graph);

		//add this graph with its score to the docList. Later we will rank it and print the answers
		//as you see, we use the graphId as identifier. 
		YosiDocument document = new YosiDocument();
		document.setDocno(graphId);
		document.setScore(finalScore);
		docList.add(document);

		//now print the graph and go in peace
		String outputPath = this.answerGraphOutputDirectory + "/" + graphId + ".ttl";
		BlazegraphUsefulMethods.printTheDamnGraph(graph, outputPath);
	}

	/** Updates the maps about the wtf of the words in this document
	 * @throws Exception */
	protected void updateWordsContribution(VNode s, 
			Map<String, Double> unigramMap, 
			Map<String, Double> bigramMap,
			double rootStaticScore,
			double staticWeight) throws Exception {
		//now we work on the words of this node
		String nodeText = s.getNodeText();
		//work with terrier - take all the words in this document, index this node document
		MemoryIndex nodeIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(nodeText);
		System.setProperty("tokeniser", "EnglishTokeniser");
		List<String> documentWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(nodeText);

		//NB: getDirectIndex not implemented, returns null
		//		PostingIndex<Pointer> di = (PostingIndex<Pointer>) nodeIndex.getDirectIndex();
		//		DocumentIndex doi = nodeIndex.getDocumentIndex();
		Lexicon<String> lex = nodeIndex.getLexicon();
		//list to keep track of the already visited words (do you know Terrier
		//has not implementation for the getDirectIndex for the MemoryIndex? Terrific.)
		Set<String> alreadyCheckedWords = new HashSet<String>();
		for(String word : documentWords) {
			if(!alreadyCheckedWords.contains(word)) {
				alreadyCheckedWords.add(word);
				if(word!=null) {
					LexiconEntry lee = lex.getLexiconEntry(word);
					if(lee==null)
						continue;
					int freq = lee.getFrequency();
					double score = this.computeGaussianWeight(freq, rootStaticScore, staticWeight);
					Double oldScore = unigramMap.get(word);
					if(oldScore == null) {
						unigramMap.put(word, score);
					} else {
						unigramMap.put(word, oldScore + score); 
					}
				}
			}
		}

		nodeIndex.close();

		//do the same for the bigrams
		nodeIndex = TerrierUsefulMethods.getBigramMemoryIndexFromDocument(nodeText);
		System.setProperty("tokeniser", "BigramTokeniser");
		documentWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(nodeText);
		lex = nodeIndex.getLexicon();

		for(String word : documentWords) {
			if(!alreadyCheckedWords.contains(word)) {
				alreadyCheckedWords.add(word);
				if(word!=null) {
					LexiconEntry lee = lex.getLexiconEntry(word);
					if(lee==null)
						continue;
					int freq = lee.getFrequency();
					double score = this.computeGaussianWeight(freq, rootStaticScore, staticWeight);
					Double oldScore = bigramMap.get(word);
					if(oldScore == null) {
						bigramMap.put(word, score);
					} else {
						bigramMap.put(word, oldScore + score); 
					}
				}
			}
		}

		System.setProperty("tokeniser", "EnglishTokeniser");

		nodeIndex.close();
		//completed the update of the scores
	}

	/** computes the contribution of a word to the wtf of that 
	 * word in this document in this position
	 * */
	protected double computeGaussianWeight(int frequency, 
			double rootStaticScore, 
			double staticWeight) {
		double score = 0;
		//argument of the exponent
		double arg = (staticWeight - rootStaticScore);
		arg = Math.pow(arg, 2);
		arg = -arg;
		arg = (double) arg / 2;
		double exp = Math.exp(arg);
		score = exp * frequency;

		return score;
	}

	/** Compute the lengths of the current document*/
	protected Pair<Double, Double> computeTheLengths(Map<String, Double> unigramMap, 
			Map<String, Double> bigramMap) {
		double uLength = 0;
		for(Entry<String, Double> e : unigramMap.entrySet()) {
			uLength += e.getValue();
		}

		double bLength = 0;
		for(Entry<String, Double> e : bigramMap.entrySet()) {
			bLength += e.getValue();
		}

		return new MutablePair<Double, Double>(uLength, bLength);
	}

	/** Executes the scoring of the answer graph 
	 * <p>
	 * NB: in the dirichlet smoothing,
	 * I am using the virtual collection as support, both 
	 * considering V and both when computing alpha.
	 * Maybe to improve performances I will try to
	 * firstly print all the answer graphs and then do the
	 * computation with the answer collection.
	 * @throws IOException 
	 * */
	protected double scoreThisGraph(Map<String, Double> unigramMap, Map<String, Double> bigramMap, 
			List<String> queryWords,
			List<String> queryWordsBigram,
			Pair<Double, Double> lengths,
			Model graph) throws IOException {

		double score = 0;
		Pair<Double, Double> alphas = new MutablePair<Double, Double>(); 

		try {
			alphas = computeAlphas(graph);
		} catch (Exception e) {
			e.printStackTrace();
		}

		//sum over all the query words
		for(String queryWord : queryWords) {
			score += this.scoreOneQueryWordUnigram(unigramMap, bigramMap, queryWord, alphas, lengths);
		}

		for(String queryWord : queryWordsBigram) {
			score += this.scoreOneQueryWordBigram(bigramMap, queryWord, alphas.getRight(), lengths.getRight());
		}

		return score;
	}

	protected Pair<Double, Double> computeAlphas(Model graph) throws Exception {
		//first alpha for unigrams
		double lambda = this.unigramClusterIndex.getCollectionStatistics().getAverageDocumentLength();

		//get |x|, the length of this document
		String graphDoc = BlazegraphUsefulMethods.fromGraphToDocument(graph);
		MemoryIndex idx = TerrierUsefulMethods.getMemoryIndexFromDocument(graphDoc);
		double x = idx.getCollectionStatistics().getAverageDocumentLength();
		idx.close();

		double alpha_u = (double) lambda / (lambda + x);

		//first alpha for unigrams
		lambda = this.bigramClusterIndex.getCollectionStatistics().getAverageDocumentLength();
		//get |x|, the length of this document
		idx = TerrierUsefulMethods.getBigramMemoryIndexFromDocument(graphDoc);
		x = idx.getCollectionStatistics().getAverageDocumentLength();
		idx.close();

		double alpha_b = (double) lambda / (lambda + x);

		Pair<Double, Double> alphas = new MutablePair<Double, Double>(alpha_u, alpha_b);
		return alphas;
	}

	protected double scoreOneQueryWordUnigram(Map<String, Double> unigramMap, 
			Map<String, Double> bigramMap, 
			String queryWord,
			Pair<Double, Double> alphas,
			Pair<Double, Double> lengths) {

		//***** UNIGRAM *****
		double alpha_u = alphas.getLeft();
		//get the wtf for this word
		Double wtf_q_v = unigramMap.get(queryWord);
		if(wtf_q_v == null)
			wtf_q_v = 0.0;
		double uLength = lengths.getLeft();
		double p1 = (double) wtf_q_v / uLength;

		Lexicon<String> lex = this.unigramClusterIndex.getLexicon();
		LexiconEntry le = lex.getLexiconEntry(queryWord);
		int num = 0;
		if(le!=null) {
			num = le.getFrequency();
		}
		double den = this.unigramClusterIndex.getCollectionStatistics().getNumberOfTokens();
		double p2 =(double) num / den;

		double logArgument = (1 - alpha_u) * p1 + (alpha_u) * p2;
		if(logArgument==0)
			return 0;
		return Math.log(logArgument);

	}

	protected double scoreOneQueryWordBigram(Map<String, Double> bigramMap,
			String queryWordBigram,
			double alpha,
			double length) {

		//*** BIGRAM *****
		//get the wtf for this query word
		Double wtf_q_v = 0.0;
		if(bigramMap.get(queryWordBigram) != null)
			wtf_q_v = bigramMap.get(queryWordBigram);
		double p1 = (double) wtf_q_v / length;

		Lexicon<String> lex = this.bigramClusterIndex.getLexicon();
		LexiconEntry le = lex.getLexiconEntry(queryWordBigram);
		int num = 0;
		if(le!=null) {
			num = le.getFrequency();
		}
		double den = this.bigramClusterIndex.getCollectionStatistics().getNumberOfTokens();
		double p2 =(double) num / den;

		double logArgument = (1 - alpha) * p1 + (alpha) * p2;
		if(logArgument==0)
			return 0;//otherwise we have -Infinity, so eliminate this contribution
		return Math.log(logArgument);

	}

	/** test main*
	 */
	public static void test(String[] args) {
		VirtualDocAndPruningOnlinePhase execution = new VirtualDocAndPruningOnlinePhase();
		execution.smartTraversing();
	}


	public String getAnswerGraphsDirectory() {
		return beforePruningAnswerGraphsDirectory;
	}


	public void setAnswerGraphsDirectory(String answerGraphsDirectory) {
		this.beforePruningAnswerGraphsDirectory = answerGraphsDirectory;
	}


	public String getRunFile() {
		return runFile;
	}


	public void setRunFile(String runFile) {
		this.runFile = runFile;
	}


	public int getLimit() {
		return limit;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}


	public String getAnswerGraphOutputDirectory() {
		return answerGraphOutputDirectory;
	}


	public void setAnswerGraphOutputDirectory(String answerGraphOutputDirectory) {
		this.answerGraphOutputDirectory = answerGraphOutputDirectory;
	}


	public String getQuery() {
		return query;
	}


	public void setQuery(String query) {
		this.query = query;
	}


	public String getBeforePruningAnswerGraphsDirectory() {
		return beforePruningAnswerGraphsDirectory;
	}


	public void setBeforePruningAnswerGraphsDirectory(String beforePruningAnswerGraphsDirectory) {
		this.beforePruningAnswerGraphsDirectory = beforePruningAnswerGraphsDirectory;
	}


	public String getUnigramIndexPath() {
		return unigramIndexPath;
	}


	public void setUnigramIndexPath(String unigramIndexPath) {
		this.unigramIndexPath = unigramIndexPath;
	}


	public String getBigramIndexPath() {
		return bigramIndexPath;
	}


	public void setBigramIndexPath(String bigramIndexPath) {
		this.bigramIndexPath = bigramIndexPath;
	}


	public String getResultOutputFile() {
		return resultOutputFile;
	}


	public void setResultOutputFile(String resultOutputFile) {
		this.resultOutputFile = resultOutputFile;
	}


	public String getQueryId() {
		return queryId;
	}


	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getReadingGraphsDirectory() {
		return readingGraphsDirectory;
	}

	public void setReadingGraphsDirectory(String readingGraphsDirectory) {
		this.readingGraphsDirectory = readingGraphsDirectory;
	}
}
