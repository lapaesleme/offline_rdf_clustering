package it.unipd.dei.ims.clustering;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;

import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.UrlUtilities;

/** In this class we perform the clustering of a RDF database.
 * We use Blazegraph as supporting library to deal with the RDF
 * and JDBC to deal with the saving of data on a support database.
 * 
 * */
public class BlazegraphClusterEngineWithDatabaseSupport {

	private static final String SQL_SELECT_SOURCE_NODES = "SELECT node_name FROM node WHERE out_degree >= ?";
	private static final String SQL_SELECT_IRI_SOURCE_NODES = "SELECT node_name FROM node WHERE iri_out_degree >= ?";

	private static final String SQL_SELECT_TERMINAL_NODES = "SELECT node_name FROM node WHERE in_degree >= ?";
	
	private static final String SQL_INSERT_SOURCE_NODE = "INSERT INTO public.source_node(" + 
			"	node_name, color)" + 
			"	VALUES (?, ?);";
	
	private static final String SQL_INSERT_TERMINAL_NODE = "INSERT INTO public.terminal_node(" + 
			"	node_name)" + 
			"	VALUES (?);";
	
	private static final String SQL_SELECT_SOURCE_NODE_COLOR ="SELECT color" + 
			"	FROM public.source_node where node_name=?;";
	

	private static final String SQL_SELECT_SOURCE_NODE ="SELECT *" + 
			"	FROM public.source_node where node_name=?;";
	
	private static final String SQL_SELECT_TERMINAL_NODE ="SELECT *" + 
			"	FROM public.terminal_node where node_name=?;";
	
	
	private static final String SQL_UPDATE_SOURCE_COLOR = "UPDATE public.source_node" + 
			"	SET node_name=?, color=?" + 
			"	WHERE node_name=?;";


	/**Repository representing where the RDF graph is stored.
	 * */
	private String repository;

	private int totalBlackNodes; 

	/**Set of the source nodes (high out degree)*/
	private Set<String> sourceSet;
	/**Set of the terminal nodes (high in degree)*/
	private Set<String> terminalSet;
	/**Map to keep track of the color of the nodes*/
	private Map<String, Color> nodeColorMap;

	/** The url of the database where to connect. An example is: 
	 * jdbc:postgresql://localhost:5432/disgenet?user=postgres&password=password*/
	private String jdbcString; 

	/** String explaining what driver to use. An example is com.mysql.jdbc.Driver*/
	private String jdbcDriver;

	/**threshold values to decide if a node belongs to set T and/or set S or neither*/
	private int lambda_in=0, lambda_out=0;

	/**Directory where to save the clusters*/
	private String destinationDirectory;

	private static int counter;
	private static int directoryCounter;

	public enum Color {
		white,
		gray,
		black
	}

	public BlazegraphClusterEngineWithDatabaseSupport () {
		sourceSet = new HashSet<String>();
		terminalSet = new HashSet<String>();
		nodeColorMap = new HashMap<String, Color>();
	}

	/**
	 * @param rdfRepository path to the RDF file.
	 * @param database String jdbc to connect to the supporting RDB database.
	 * @param driver Jdbc driver used.*/
	public BlazegraphClusterEngineWithDatabaseSupport (String rdfRepository, String database, String driver, 
			int lin, int lout, String destDirectory) {
		this.repository = rdfRepository;
		sourceSet = new HashSet<String>();
		terminalSet = new HashSet<String>();
		nodeColorMap = new HashMap<String, Color>();
		lambda_in = lin;
		lambda_out = lout;
		jdbcString = database;
		jdbcDriver = driver;
		destinationDirectory = destDirectory;
	}


	/**Method to cluster the graph represented.
	 * 
	 * This method uses only the Relational Database
	 * 
	 * @param labelFlag if set to true, count the Literals as neighbours when computing the out degree of a node
	 * */
	public void clusterAlgorithm(int tau, List<String> connectivityList, boolean labelFlag) {
		//counter of how many clusters we have written
		counter = 0;
		directoryCounter = 0;

		
		Connection connection = null;

		try {
			//open RDB connection
			connection = DriverManager.getConnection(jdbcString);

			//prepare the source and terminal nodes
			this.computeNodesSets(connection, labelFlag);

			//start the real clustering part
			this.realClustering(connection, connectivityList, tau);

		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			try {
				if (connection != null)
					connection.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}



	}

	/** Performs the real clustering. 
	 * 
	 * */
	private void realClustering(Connection connection, List<String> connectivityList, int tau) {

		//clean the directory where we are going to write the clusters
		try {
			FileUtils.cleanDirectory(new File(this.destinationDirectory));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}

		//get the source nodes
		Iterator<String> sourceIterator = sourceSet.iterator();
		int totalSourceNodes = sourceSet.size();
		totalBlackNodes = 0;

		while(sourceIterator.hasNext()) {
			String source = sourceIterator.next();
			if(source.equals(""))
				//ignore the empty node
				continue;

			//take the color
			Color sourceColor = nodeColorMap.get(source);
			Stopwatch timer = Stopwatch.createStarted();
			if(sourceColor != Color.black) {
				//create the cluster

				//				Model cluster = this.extendCluster2(rdfConnection, source, tau, connectivityList);
				Model cluster = this.extendClusterWithTripleStoreSupport(connection, source, tau, connectivityList);
				this.printTheDamnCluster(cluster, timer);
			}
			if(totalBlackNodes%10000==0) {
				System.out.println("MONITOR: visited " + totalBlackNodes + " out of " + totalSourceNodes + " source nodes. "
						+ "Current directory " + directoryCounter);
			}
		}
	}
	
	private void realClusteringWithFullDBSupport(Connection connection, List<String> connectivityList, int tau) throws SQLException {

		//clean the directory where we are going to write the clusters
		try {
			FileUtils.cleanDirectory(new File(this.destinationDirectory));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}

		//get the source nodes
		Iterator<String> sourceIterator = sourceSet.iterator();
		int totalSourceNodes = sourceSet.size();
		totalBlackNodes = 0;

		while(sourceIterator.hasNext()) {
			String source = sourceIterator.next();
			if(source.equals(""))
				//ignore the empty node
				continue;

			//take the color
			PreparedStatement stmt = connection.prepareStatement(SQL_SELECT_SOURCE_NODE_COLOR);
			stmt.setString(1, source);
			ResultSet rs = stmt.executeQuery();
			String color = "white";
			if(rs.next()) {
				color = rs.getString(1);
			}
			
			Stopwatch timer = Stopwatch.createStarted();
			if(color != "white") {
				//create the cluster

				Model cluster = this.extendClusterWithFullDBSupport(connection, source, tau, connectivityList);
				this.printTheDamnCluster(cluster, timer);
			}
			if(totalBlackNodes%10000==0) {
				System.out.println("MONITOR: visited " + totalBlackNodes + " out of " + totalSourceNodes + " source nodes. "
						+ "Current directory " + directoryCounter);
			}
		}
	}

	/** This methods starts from a source node, represented by a string, and expands it following
	 * the clustering agorithm.
	 * *<p> 
	 *This is the third version of the clustering algorithm, born because of the following
	 *considerations:
	 *</p> 
	 *
	 *<p>
	 *The node which have great out-degree should be the starting points of the algorithm. They are called 
	 *<b>source nodes</b>. The nodes which have great in-degree are probably nodes with useful information
	 *for many clusters. They should be used but not further explored. They are called <b>terminal nodes</b>.
	 *a node can be both source and terminal.
	 *</p>
	 *
	 *<p>
	 *A node which is neither a source node nor a terminal node is called <p>accessory node</p>, and we
	 *assume it carries information which is of little relevance to the cluster, but that can be
	 *useful for some queries, so we include it.
	 *</p>
	 *
	 *<p>
	 *Note that a literal node not necessarily is the object of only one triple. It can be the object of various 
	 *triples. That means that the literals that are terminal nodes are very useful for the cluster. Moreover, 
	 *often a terminal node is an IRI which contains only an ID. To obtain useful information, it is necessary
	 *to include in the cluster other nodes around the terminal nodes that are literals and better describe its nature.
	 *</p>
	 *
	 *N.B. A little slow to perform. And I discovered that with certain URL
	 *the listStatements method doesn't work, making impossible to create
	 *the correct clusters.
	 *See method extendCluster2
	 *
	 * @param source the starting node to build the cloud
	 * @param tau the radius of the cloud
	 * @param connectivityList list of Strings representing the labels of the edges we will explore
	 * @throws RepositoryException 
	 * @throws QueryEvaluationException 
	 * */
	@SuppressWarnings({ "unused" })
	@Deprecated
	private Model extendCluster(RepositoryConnection connection, String source, int tau, List<String> connectivityList) {
		//create a new graph
		Model cluster = new TreeModel ();

		//queue to keep track of the nodes that has to be explore yet (the information kept
		//is the node and the radius tau that we still have)
		Queue<Pair<String, Integer>> extendingQueue = new LinkedList<Pair<String, Integer>>();

		extendingQueue.add(new MutablePair<String, Integer>(source, tau));

		//visit the cluster
		while(! extendingQueue.isEmpty()) {
			Pair<String, Integer> sPair = extendingQueue.remove();
			//take the central node
			String s = sPair.getLeft();
			//mark the source node s as visited (thus the clustering algorithm will end)
			nodeColorMap.put(s, Color.black);
			totalBlackNodes++;

			//keep track of how far off we are from the origin (update the radius)
			int radius = sPair.getRight() - 1;

			//look at the neighborhood
			RepositoryResult<Statement> iterator = null;
			try {
				//query the graph about the nodes around the subject
				iterator = BlazegraphUsefulMethods.listStatements(connection, s);
				while(iterator.hasNext()) {
					//iterate over the neighborhood
					Statement triple = iterator.next();

					//get the object
					Value obj = triple.getObject();
					Value predicate = triple.getPredicate();

					Color objColor = nodeColorMap.get(obj.toString());

					//ACCESSORY CLOUD
					//if is a simple accessory node
					if(!sourceSet.contains(obj.toString()) && !terminalSet.contains(obj.toString())) {
						//you can add the statement to the cluster
						cluster.add(triple);
					} else if (terminalSet.contains(obj.toString())) {
						//else it is a terminal node, useful information anyway
						cluster.add(triple);
						if(obj instanceof URI) {
							//if it is an URI, it can have other useful information to include
							RepositoryResult<Statement> objIterator = 
									BlazegraphUsefulMethods.listStatements(connection, obj.toString());

							while(objIterator.hasNext()) {
								Statement t = objIterator.next();
								Value v = t.getObject();
								if(v instanceof Literal || 
										(!sourceSet.contains(v.toString()) && !terminalSet.contains(v.toString()) ) ) {
									cluster.add(t);
								}
							}
							objIterator.close();
						}
					} //END ACCESSORY CLOUD
					else if((radius >0) &&
							(objColor != Color.black) &&
							connectivityList.contains(predicate.toString())) {
						//'walking phase' of the clustering
						//if the edge is explorable, the radius is still greater than 0 and the node u has not already been
						//used as central node
						if(sourceSet.contains(obj.toString()) && 
								!terminalSet.contains(obj.toString())) {
							//if it is a source node and not a terminal node
							Pair<String, Integer> uPair = new MutablePair<String, Integer>(obj.toString(), radius);
							extendingQueue.add(uPair);
							//mark as visited
							nodeColorMap.put(obj.toString(), Color.black);
							totalBlackNodes++;
							cluster.add(triple);
						}
					}//END of the walking phase
				}//end of the iteration over the current node
			}  catch (RepositoryException e) {
				e.printStackTrace();
			} finally {
				if(iterator != null)
					try {
						iterator.close();
					} catch (RepositoryException e) {
						e.printStackTrace();
					}
			}

		}//end of the cluster

		return cluster;
	}

	/** Method to extend the clusters that only uses the table triple_store in memory.
	 * */
	private Model extendClusterWithTripleStoreSupport(Connection cxn, String source, int tau, List<String> connectivityList) {
		//create a new graph
		Model cluster = new TreeModel ();

		//queue to keep track of the nodes that has to be explore yet (the information kept
		//is the node and the radius tau that we still have)
		Queue<Pair<String, Integer>> extendingQueue = new LinkedList<Pair<String, Integer>>();

		extendingQueue.add(new MutablePair<String, Integer>(source, tau));

		//visit the cluster
		while(! extendingQueue.isEmpty()) {
			Pair<String, Integer> sPair = extendingQueue.remove();
			//take the central node
			String s = sPair.getLeft();
			//mark the source node s as visited (thus the clustering algorithm will end)
			nodeColorMap.put(s, Color.black);
			totalBlackNodes++;

			//keep track of how far off we are from the origin (update the radius)
			int radius = sPair.getRight() - 1;

			//look at the neighborhood
			try {
				//query the graph about the nodes around the subject
				List<Statement> list = this.getNeighboursViaDB(cxn, s);
				for(Statement triple : list) {
					//iterate over the neighborhood

					//get the object
					Value obj = triple.getObject();
					Value predicate = triple.getPredicate();

					Color objColor = nodeColorMap.get(obj.toString());

					//ACCESSORY CLOUD
					//if it is a simple accessory node
					if(!sourceSet.contains(obj.toString()) && !terminalSet.contains(obj.toString())) {
						//you can add the statement to the cluster
						cluster.add(triple);
					} else if (terminalSet.contains(obj.toString())) {
						//else it is a terminal node, useful information anyway
						cluster.add(triple);
						if(obj instanceof URI) {
							//if it is an URI, it can have other useful information to include
							List<Statement> objList = this.getNeighboursViaDB(cxn, obj.toString());

							for(Statement t : objList ) {
								Value v = t.getObject();
								if(v instanceof Literal || 
										(!sourceSet.contains(v.toString()) && !terminalSet.contains(v.toString()) ) ) {
									cluster.add(t);
								}
							}
						}
					} //END ACCESSORY CLOUD
					else if((radius >0) &&
							(objColor != Color.black) &&
							connectivityList.contains(predicate.toString())) {
						//'walking phase' of the clustering
						//if the edge is explorable, the radius is still greater than 0 and the node u has not already been
						//used as central node
						if(sourceSet.contains(obj.toString()) && 
								!terminalSet.contains(obj.toString())) {
							//if it is a source node and not a terminal node
							Pair<String, Integer> uPair = new MutablePair<String, Integer>(obj.toString(), radius);
							extendingQueue.add(uPair);
							//mark as visited
							cluster.add(triple);
						}
					}//END of the walking phase
				}//end of the iteration over the current node
			}  catch (SQLException e) {
				e.printStackTrace();
			} 

		}//end of the cluster

		return cluster;
	}
	
	
	/** Method to extend the clusters that only uses the table triple_store in memory.
	 * <p>
	 * Also, it uses the tables source_node and terminal_node to look up and update the source nodes
	 * and terminal nodes.
	 * @throws SQLException 
	 * */
	private Model extendClusterWithFullDBSupport(Connection cxn, String source, int tau, List<String> connectivityList) 
			throws SQLException {
		
		
		PreparedStatement updateSourceColor = cxn.prepareStatement(SQL_UPDATE_SOURCE_COLOR);
		//create a new graph
		Model cluster = new TreeModel ();

		//queue to keep track of the nodes that has to be explore yet (the information kept
		//is the node and the radius tau that we still have)
		Queue<Pair<String, Integer>> extendingQueue = new LinkedList<Pair<String, Integer>>();

		extendingQueue.add(new MutablePair<String, Integer>(source, tau));

		//visit the cluster
		while(! extendingQueue.isEmpty()) {
			Pair<String, Integer> sPair = extendingQueue.remove();
			//take the central node
			String s = sPair.getLeft();
			
			//mark the source node s as visited (thus the clustering algorithm will end)
			updateSourceColor.setString(1, s);
			updateSourceColor.setString(2, "black");
			updateSourceColor.setString(3, s);
			updateSourceColor.executeQuery();
			totalBlackNodes++;

			//keep track of how far off we are from the origin (update the radius)
			int radius = sPair.getRight() - 1;

			//look at the neighborhood
			try {
				//query the graph about the nodes around the subject
				List<Statement> list = this.getNeighboursViaDB(cxn, s);
				for(Statement triple : list) {
					//iterate over the neighborhood

					//get the object
					Value obj = triple.getObject();
					Value predicate = triple.getPredicate();
					
					//get the color of the object node
					PreparedStatement stmt = cxn.prepareStatement(SQL_SELECT_SOURCE_NODE_COLOR);
					stmt.setString(1, obj.toString());
					ResultSet rs = stmt.executeQuery();
					
					String oColor = "black";
					if(rs.next()) {
						oColor = rs.getString(1);
					}
					
					//check if the obj is a source node and/or a terminal node
					PreparedStatement checkIfSourceNode = cxn.prepareStatement(SQL_SELECT_SOURCE_NODE);
					PreparedStatement checkIfTerminalNode = cxn.prepareStatement(SQL_SELECT_TERMINAL_NODE);
					
					checkIfSourceNode.setString(1, obj.toString());
					checkIfTerminalNode.setString(1, obj.toString());
					
					ResultSet snRs = checkIfSourceNode.executeQuery();
					boolean sN = snRs.next();
					
					ResultSet tnRs = checkIfTerminalNode.executeQuery();
					boolean tN = tnRs.next();
					

					//ACCESSORY CLOUD
					//if it is a simple accessory node
					if(!sN && !tN) {
						//you can add the statement to the cluster
						cluster.add(triple);
					} else if (tN) {
						//else it is a terminal node, useful information anyway
						cluster.add(triple);
						if(obj instanceof URI) {
							//if it is an URI, it can have other useful information to include
							List<Statement> objList = this.getNeighboursViaDB(cxn, obj.toString());

							for(Statement t : objList ) {
								Value v = t.getObject();
								//check if v is a source node and/or a terminal node
								PreparedStatement checkIfVSourceNode = cxn.prepareStatement(SQL_SELECT_SOURCE_NODE);
								PreparedStatement checkIfVTerminalNode = cxn.prepareStatement(SQL_SELECT_TERMINAL_NODE);
								
								checkIfVSourceNode.setString(1, v.toString());
								checkIfVTerminalNode.setString(1, v.toString());
								
								ResultSet snRsV = checkIfVSourceNode.executeQuery();
								boolean sNv = snRsV.next();
								
								ResultSet tnRsV = checkIfVTerminalNode.executeQuery();
								boolean tNv = tnRsV.next();
								if(v instanceof Literal || 
										(!sNv && !tNv ) ) {
									cluster.add(t);
								}
							}
						}
					} //END ACCESSORY CLOUD
					else if((radius >0) &&
							(oColor != "black") &&
							connectivityList.contains(predicate.toString())) {
						//'walking phase' of the clustering
						//if the edge is explorable, the radius is still greater than 0 and the node u has not already been
						//used as central node
						if(sN && !tN) {
							//if it is a source node and not a terminal node
							Pair<String, Integer> uPair = new MutablePair<String, Integer>(obj.toString(), radius);
							extendingQueue.add(uPair);
							//mark as visited
							cluster.add(triple);
						}
					}//END of the walking phase
				}//end of the iteration over the current node
			}  catch (SQLException e) {
				e.printStackTrace();
			} 

		}//end of the cluster

		return cluster;
	}

	/** Returns a List of Statements that are the triples with 
	 * source the node s. This method uses a copy of the RDF database
	 * inside a Relational Database in order to be quick.
	 * @throws SQLException 
	 * */
	private List<Statement> getNeighboursViaDB(Connection connection, String s) throws SQLException {
		String query = "SELECT subject_, predicate_, object_" + 
				"	FROM public.triple_store WHERE ( subject_=? );";
		PreparedStatement stmt = connection.prepareStatement(query);
		stmt.setString(1, s);
		ResultSet rs = stmt.executeQuery();

		List<Statement> list = new ArrayList<Statement>();
		while(rs.next()) {
			String sbj = rs.getString(1);
			String pr = rs.getString(2);
			String obj = rs.getString(3);

			URI subject = new URIImpl(sbj);
			Value predicate = new URIImpl(pr);
			Value object;
			if(UrlUtilities.checkIfValidURL(obj)) {
				//obj is a URL
				object = new URIImpl(obj);
			}
			else {
				object = dealWithTheObjectLiteralString(obj);
			}
			Statement stat = new StatementImpl(subject, (URI) predicate, object);
			list.add(stat);
		}
		return list;
	}

	/** When we read a literal from the relational database, we need to recreate it exactly for 
	 * the creation of a RDF graph. 
	 * */
	private Literal dealWithTheObjectLiteralString(String obj) {
		String regex = "\"(.*)\"(\\^\\^<(.*)>)?(@(.*))?";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(obj);
		if(matcher.find()) {
			String firstString = matcher.group(1);
			String uriString = matcher.group(3);
			String languageString = matcher.group(5); 
			if(uriString == null && languageString == null) {
				//only a single string, nothing else
				Literal literal = new LiteralImpl(firstString);
				return literal;
			}
			else if (uriString!= null && languageString == null) {
				//a literal with its type after ^^
				Literal literal;
				try {
					URI uri = new URIImpl(uriString);
					literal = new LiteralImpl(firstString, uri);
				}
				catch(Exception e) {
					System.err.println("Strange url: " + uriString);
					literal = new LiteralImpl(firstString+uriString);
				}
				return literal;
			}
			else if (uriString == null && languageString != null) {
				Literal literal = new LiteralImpl(firstString, languageString);
				return literal;
			}
		} else {
			Literal literal = new LiteralImpl(obj);
			return literal;
		}
		return null;
	}

	/** Like {@link extendCluster}, but it uses the listTriples method.
	 * Still a little too slow.
	 * */
	private Model extendCluster2(RepositoryConnection connection, String source, int tau, List<String> connectivityList) {
		//create a new graph
		Model cluster = new TreeModel ();

		//queue to keep track of the nodes that has to be explore yet (the information kept
		//is the node and the radius tau that we still have)
		Queue<Pair<String, Integer>> extendingQueue = new LinkedList<Pair<String, Integer>>();

		extendingQueue.add(new MutablePair<String, Integer>(source, tau));

		//visit the cluster
		while(! extendingQueue.isEmpty()) {
			Pair<String, Integer> sPair = extendingQueue.remove();
			//take the central node
			String s = sPair.getLeft();
			//mark the source node s as visited (thus the clustering algorithm will end)
			nodeColorMap.put(s, Color.black);
			totalBlackNodes++;

			//keep track of how far off we are from the origin (update the radius)
			int radius = sPair.getRight() - 1;

			//look at the neighborhood
			try {
				//query the graph about the nodes around the subject
				TupleQueryResult result = BlazegraphUsefulMethods.listTriples(connection, s);
				while(result.hasNext()) {
					//iterate over the neighborhood
					BindingSet triple = result.next();

					URI subj = new URIImpl(s);
					Value obj = triple.getValue("o");
					Value predicate = triple.getValue("p");
					Statement stat = new StatementImpl(subj, (URI) predicate, obj);

					Color objColor = nodeColorMap.get(obj.toString());

					//ACCESSORY CLOUD
					//if is a simple accessory node
					if(!sourceSet.contains(obj.toString()) && !terminalSet.contains(obj.toString())) {
						//you can add the statement to the cluster
						cluster.add(stat);
					} else if (terminalSet.contains(obj.toString())) {
						//else it is a terminal node, useful information anyway
						cluster.add(stat);
						if(obj instanceof URI) {
							//if it is an URI, it can have other useful information to include
							TupleQueryResult objResult = BlazegraphUsefulMethods.listTriples(connection, obj.toString());
							while(objResult.hasNext()) {
								BindingSet bs = objResult.next();
								Value v = bs.getValue("o");
								if(v instanceof Literal || 
										(!sourceSet.contains(v.toString()) && !terminalSet.contains(v.toString()) ) ) {
									Value pred_ = bs.getValue("p");
									Statement stat_ = new StatementImpl((URI) obj, (URI) pred_, v);
									cluster.add(stat_);
								}
							}
							objResult.close();
						}
					} //END ACCESSORY CLOUD
					else if((radius >0) &&
							(objColor != Color.black) &&
							connectivityList.contains(predicate.toString())) {
						//'walking phase' of the clustering
						//if the edge is explorable, the radius is still greater than 0 and the node u has not already been
						//used as central node
						if(sourceSet.contains(obj.toString()) && 
								!terminalSet.contains(obj.toString())) {
							//if it is a source node and not a terminal node
							Pair<String, Integer> uPair = new MutablePair<String, Integer>(obj.toString(), radius);
							extendingQueue.add(uPair);
							//mark as visited
							nodeColorMap.put(obj.toString(), Color.black);
							totalBlackNodes++;
							cluster.add(stat);
						}
					}//END of the walking phase
				}//end of the iteration over the current node
			} catch (QueryEvaluationException e) {
				e.printStackTrace();
			}  finally {

			}

		}//end of the cluster

		return cluster;
	}


	
	/** Computes the source and terminal nodes and insert them into a Relational
	 * Database.
	 * <p>
	 * This method should be used when the number of source nodes goes beyond the 10 millions.
	 * 
	 * */
	private void computeAndStoreNodesSets(Connection connection, boolean labelFlag) {
		try {
			System.out.print("now computing and storing nodes sets...");
			//perform query to get the source nodes
			PreparedStatement preparedSelect;
			
			PreparedStatement insertSourceNodesStatement = connection.prepareStatement(SQL_INSERT_SOURCE_NODE);
			PreparedStatement insertTerminalNodesStatement = connection.prepareStatement(SQL_INSERT_TERMINAL_NODE);
			
			if(labelFlag) {
				preparedSelect = connection.prepareStatement(SQL_SELECT_SOURCE_NODES);
			}
			else
				preparedSelect = connection.prepareStatement(SQL_SELECT_IRI_SOURCE_NODES);

			//execute the query, get the source nodes
			preparedSelect.setInt(1, this.lambda_out);
			ResultSet rs = preparedSelect.executeQuery();
			int mapCounter = 0;
			int batchCounter = 0;
			
			while(rs.next()) {
				String nodeString = rs.getString(1);
				
				//add the insert to the batch
				insertSourceNodesStatement.setString(1, nodeString);
				insertSourceNodesStatement.addBatch();
				
				mapCounter++;
				batchCounter++;
				if(batchCounter > 100000) {
					//execute this batch of insertions
					batchCounter = 0;
					
					insertSourceNodesStatement.executeBatch();
					insertSourceNodesStatement.clearBatch();
					
					System.err.println("Selected " + mapCounter + " source nodes");
				}
			}
			if(batchCounter > 0) {
				batchCounter = 0;
				
				insertSourceNodesStatement.executeBatch();
				insertSourceNodesStatement.clearBatch();
			}
			System.out.println("Selected " + mapCounter + " source nodes in total");

			//perform query to get the terminal nodes
			preparedSelect = connection.prepareStatement(SQL_SELECT_TERMINAL_NODES);
			preparedSelect.setInt(1, this.lambda_in);
			rs = preparedSelect.executeQuery();

			mapCounter = 0;
			while(rs.next()) {
				String nodeString = rs.getString(1);
				
				insertTerminalNodesStatement.setString(1, nodeString);
				insertTerminalNodesStatement.addBatch();
				
				mapCounter++;
				batchCounter++;
				if(batchCounter > 100000) {
					batchCounter = 0;
					insertTerminalNodesStatement.executeBatch();
					insertTerminalNodesStatement.clearBatch();
				}
			}
			if(batchCounter > 0) {
				insertTerminalNodesStatement.executeBatch();
				insertTerminalNodesStatement.clearBatch();
			}
			System.out.print("Selected " + mapCounter + " terminal nodes");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** Populates the sets of source nodes and terminal nodes.
	 * Besides, colors all the source nodes of white.
	 * 
	 * @param labelFlag if set to false, don't count the literal as neighborhoods when computing
	 * out degree.
	 * */
	private void computeNodesSets(Connection connection, boolean labelFlag) {
		try {
			System.out.print("now computing nodes sets...");
			//perform query to get the source nodes
			PreparedStatement preparedSelect; 
			if(labelFlag) {
				preparedSelect = connection.prepareStatement(SQL_SELECT_SOURCE_NODES);
			}
			else
				preparedSelect = connection.prepareStatement(SQL_SELECT_IRI_SOURCE_NODES);

			preparedSelect.setInt(1, this.lambda_out);
			ResultSet rs = preparedSelect.executeQuery();
			int mapCounter = 0;
			while(rs.next()) {
				String nodeString = rs.getString(1);
				this.sourceSet.add(nodeString);
				this.nodeColorMap.put(nodeString, Color.white);
				mapCounter++;
				if(mapCounter%100000 == 0) {
					System.err.println("Selected " + mapCounter + " source nodes");
				}
			}
			System.out.println("Selected " + mapCounter + " source nodes in total");

			//perform query to get the terminal nodes
			preparedSelect = connection.prepareStatement(SQL_SELECT_TERMINAL_NODES);
			preparedSelect.setInt(1, this.lambda_in);
			rs = preparedSelect.executeQuery();

			mapCounter = 0;
			while(rs.next()) {
				String nodeString = rs.getString(1);
				this.terminalSet.add(nodeString);
				mapCounter++;
			}
			System.out.print("Selected " + mapCounter + " terminal nodes");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**Prints the parameter cluster rdf graph in a file using the 
	 * turtle syntax.
	 * */
	private void printTheDamnCluster(Model cluster, Stopwatch timer) {

		if(counter%2048 == 0) {
			//create a new directory in order to ease the work to the file system
			System.out.println(counter + " clusters produced in " + timer + " in directory " + directoryCounter + ""
					+ " visited " + totalBlackNodes + " black nodes");
			directoryCounter++;  
			(new File(this.destinationDirectory + directoryCounter)).mkdir();
		}

		//write down the clusters
		File f = new File(this.destinationDirectory + directoryCounter + "/cluster_" + (++counter) + ".ttl");
		try (OutputStream out = new FileOutputStream(f)){
			RDFWriter writer = Rio.createWriter(RDFFormat.TURTLE, out);
			writer.startRDF();
			for(Statement st: cluster) {
				writer.handleStatement(st);
			}
			writer.endRDF();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error in writing the cluster number "+ counter);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Close didn't worked for the output stream of " + counter);
		} catch (RDFHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}






	public String getRepository() {
		return repository;
	}


	public void setRepository(String repository) {
		this.repository = repository;
	}


	public Set<String> getSourceSet() {
		return sourceSet;
	}


	public void setSourceSet(Set<String> sourceSet) {
		this.sourceSet = sourceSet;
	}


	public Set<String> getTerminalSet() {
		return terminalSet;
	}


	public void setTerminalSet(Set<String> terminalSet) {
		this.terminalSet = terminalSet;
	}


	public Map<String, Color> getNodeColorMap() {
		return nodeColorMap;
	}


	public void setNodeColorMap(Map<String, Color> nodeColorMap) {
		this.nodeColorMap = nodeColorMap;
	}


	public String getJdbcString() {
		return jdbcString;
	}


	public void setJdbcString(String jdbcString) {
		this.jdbcString = jdbcString;
	}


	public String getJdbcDriver() {
		return jdbcDriver;
	}


	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}


	public int getLambda_in() {
		return lambda_in;
	}


	public void setLambda_in(int lambda_in) {
		this.lambda_in = lambda_in;
	}


	public int getLambda_out() {
		return lambda_out;
	}


	public void setLambda_out(int lambda_out) {
		this.lambda_out = lambda_out;
	}


	public String getDestinationDirectory() {
		return destinationDirectory;
	}


	public void setDestinationDirectory(String destinationDirectory) {
		this.destinationDirectory = destinationDirectory;
	}





}
