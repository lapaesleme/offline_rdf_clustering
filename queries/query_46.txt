46
QALD2_tr-18	Which museum exhibits The Scream by Munch?

-- The Scream Edvard Munch museum --

CONSTRUCT {<http://dbpedia.org/resource/The_Scream> <http://www.w3.org/2000/01/rdf-schema#comment> ?o0 ;  <http://www.w3.org/2000/01/rdf-schema#label> ?o1 ;  <http://dbpedia.org/property/museum> <http://dbpedia.org/resource/National_Gallery_of_Norway> ;  <http://dbpedia.org/property/artist> <http://dbpedia.org/resource/Edvard_Munch> . } WHERE {  { <http://dbpedia.org/resource/The_Scream> <http://www.w3.org/2000/01/rdf-schema#comment> ?o0 ;  <http://www.w3.org/2000/01/rdf-schema#label> ?o1 ;  <http://dbpedia.org/property/museum> <http://dbpedia.org/resource/National_Gallery_of_Norway> ;  <http://dbpedia.org/property/artist> <http://dbpedia.org/resource/Edvard_Munch> . }  }