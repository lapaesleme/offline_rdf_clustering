package it.unipd.dei.ims.clustering;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import it.unipd.dei.ims.clustering.utilities.UsefulMethods;
import it.unipd.dei.ims.statistics.Statistics;
import it.unipd.dei.ims.terrier.utilities.UsefulConstants;

/**This class creates the clusters on the RDf graph following my algorithm
 * 
 * @author Dennis D.*/
@Deprecated
public class ClusterEngine {

	/**RDF graphs to be clustered*/
	private Model model;

	private String lang;
	private String outputPathDirectory ;

	/**Set of the source nodes (high out degree)*/
	private Set<RDFNode> sourceSet;
	/**Set of the terminal nodes (high in degree)*/
	private Set<RDFNode> terminalSet;
	/**Map to keep track of the color of the nodes*/
	private Map<RDFNode, Color> nodeColorMap;
	/**This Map keeps track of how many times a node has been visited*/
	private Map<RDFNode, Integer> nodeVisitMap;

	/**threshold values to decide if a node belongs to set T and/or set S or neither*/
	private int lambda_in=0, lambda_out=0;

	public enum Color {
		white,
		gray,
		black
	}

	/**keeps track of how many cluster have been printed*/
	private int counter = 1; 
	private int directoryCounter = 1;

	private static int checkCount = 0;

	public ClusterEngine (Model m) {
		this.model = m;
		sourceSet = new HashSet<RDFNode>();
		terminalSet = new HashSet<RDFNode>();
		nodeColorMap = new HashMap<RDFNode, Color>();
		this.nodeVisitMap = new HashMap<RDFNode, Integer>();
	}

	public ClusterEngine (Model m, int lin, int lout) {
		this.model = m;
		sourceSet = new HashSet<RDFNode>();
		terminalSet = new HashSet<RDFNode>();
		nodeColorMap = new HashMap<RDFNode, Color>();
		this.nodeVisitMap = new HashMap<RDFNode, Integer>();
		this.lambda_in = lin;
		this.lambda_out = lout;
	}

	public ClusterEngine (Model m, Statistics stat, int lin, int lout) {
		this.model = m;
		sourceSet = new HashSet<RDFNode>();
		terminalSet = new HashSet<RDFNode>();
		nodeColorMap = new HashMap<RDFNode, Color>(2*stat.getEdgeSetCardinality());
		this.nodeVisitMap = new HashMap<RDFNode, Integer>(2*stat.getEdgeSetCardinality());
		this.lambda_in = lin;
		this.lambda_out = lout;
	}

	private class NodesMonitorThread extends Thread {
		public void run() {
			System.out.println("Controlled " + checkCount + " nodes");
		}
	}

	/**!!! This method must be called before starting the clustering algorithm !!!  
	 * Computes the sets T (of terminal nodes) and S (of source nodes) 
	 *  on the model of the engine.*/
	public void computeTerminalAndSourceNodesSets (Statistics stat) {
		System.out.println("finding the central nodes...");

		Map<String, String> propertyMap = UsefulMethods.getProperties();
		this.lang = propertyMap.get("model.writing.lang");
		this.outputPathDirectory = propertyMap.get("clusters.directory");

		if(this.model.isEmpty() || this.model == null) {
			throw new IllegalArgumentException("No model given");
		}

		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(new NodesMonitorThread(), 5, 30, TimeUnit.SECONDS);

		//color the nodes of white
		Map<RDFNode, Integer> inMap = stat.getInDegreeMap();

		//select the terminal nodes
		for(Entry<RDFNode, Integer> entry : inMap.entrySet()) {
			checkCount++;
			RDFNode obj = entry.getKey();
			Color value = this.nodeColorMap.get(obj);
			if(value == null) {
				//first time we see this node
				nodeColorMap.put(obj, Color.white);
				//				nodeVisitMap.put(obj, 0);
			}

			Integer inDegree = entry.getValue();
			boolean ok;
			if(inDegree != null && inDegree >= lambda_in) {
				ok = terminalSet.add(obj);
			}

		}

		Map<RDFNode, Integer> outMap = stat.getOutDegreeMap();
		for(Entry<RDFNode, Integer> entry : outMap.entrySet()) {
			checkCount++;
			RDFNode subj = entry.getKey();
			Color value = this.nodeColorMap.get(subj);
			if(value == null) {
				//first time we see this node, color it white
				nodeColorMap.put(subj, Color.white);
				//				nodeVisitMap.put(subj, 0);
			}

			Integer outDegree = entry.getValue();
			boolean ok;
			if(outDegree != null && outDegree >= lambda_out) {
				ok = sourceSet.add(subj);
			}

		}

		scheduler.shutdownNow();

		//		while(iterator.hasNext()) {
		//			Statement t = iterator.next();
		//			Resource subject = t.getSubject();
		//			Color value = this.nodeColorMap.get(subject);
		//			if(value == null) {
		//				//first time we see this node
		//				nodeColorMap.put(subject, Color.white);
		//				nodeVisitMap.put(subject, 0);
		//			}
		//
		//			RDFNode object = t.getObject();
		//			value = this.nodeColorMap.get(object);
		//			if(value == null) {
		//				nodeColorMap.put(object, Color.white);
		//				nodeVisitMap.put(object, 0);
		//			}
		//		}//end whitewashing
		//		System.out.println("end of whitewashing phase");

		/*NB: reference for the future: a possibility is that the empty string is 
		 * used as RDF node to represent absence of information. THis is a strange
		 * choice, and the Set implementation in Java, when we try to introduce an element
		 * such as a Node which corresponds to an empty string,
		 * refuses to accept it. 
		 * 
		 * Usually the empty string (not a blank node, a node with an empty string) 
		 * is used as object, but it's strange, is like saying: this information is missing.
		 * So such a triple shouldn't even be present in the database because we 
		 * are not interested in "non information".
		 * 
		 * For now the choice is to ignore such node, removing 
		 * it from the graph.*/
		//		Map<RDFNode, Integer> inDegreeMap = stat.getInDegreeMap();
		//		Map<RDFNode, Integer> outDegreeMap = stat.getOutDegreeMap();
		//
		//		//for each and every node, let's decide where to put him
		//		for( RDFNode node : nodeColorMap.keySet()) {
		//			Integer inDegree = inDegreeMap.get(node);
		//			boolean ok;
		//			if(inDegree != null && inDegree >= lambda_in) {
		//				ok = terminalSet.add(node);
		//			}
		//
		//			Integer outDegree = outDegreeMap.get(node);
		//			if(outDegree != null && outDegree >= lambda_out) {
		//				ok = sourceSet.add(node);
		//			}
		//		}
		System.out.println("central nodes: done");
	}

	/**Creates the clusters. It is necessary to execute this method
	 * after the setup performed in @link{terminalAndSourceSetBuilding}
	 * 
	 * @param tau the radius of the cloud
	 * @param destination path to the directory where to print the damn clusters
	 * */
	public List<Model> clustering(int tau, List<String> connectivityList, String destination) {
		Iterator<RDFNode> sourceIterator = sourceSet.iterator();
		List<Model> clusterList = new ArrayList<Model>();
		boolean first = true;
		while(sourceIterator.hasNext()) {
			RDFNode source = sourceIterator.next();
			if(source.toString().equals(""))
				//ignore the empty node
				continue;

			//take the color of this node
			Color sourceColor = nodeColorMap.get(source);
			if(sourceColor != Color.black) {
				//keep rockin' (create a new cluster)
				Model cluster = this.extendCluster(source, tau, connectivityList);
				this.printTheDamnCluster(cluster, first, destination);
				first = false;
			}
		}
		return clusterList;
	}

	/**Prints the parameter cluster rdf graph in a file using the 
	 * language specified in the properties in the path also specified in the properties.
	 * */
	private void printTheDamnCluster(Model cluster, boolean first, String destination) {
		if(first) {
			//clean the directory
			try {
				FileUtils.cleanDirectory(new File(destination));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.err.println("unable to clean the directory");
			}
			//create the first sub-directory
			(new File(destination + directoryCounter)).mkdir();
		}

		if(this.counter%2048 == 0) {
			//create a new directory in order to ease the work to the file system
			System.out.println(counter + " clusters produced");
			directoryCounter++;
			(new File(destination + directoryCounter)).mkdir();
		}

		//write down the clusters
		File f = new File(destination + directoryCounter + "/cluster_" + counter + ".nt");
		try (OutputStream out = new FileOutputStream(f)){
			cluster.write(out, lang);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error in writing the cluster number "+ counter);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Close didn't worked for the output stream of " + counter);
		}
		counter++;
	}


	/**Expands 1 cluster following my algorithm.
	 *<p> 
	 *This is the third version of the clustering algorithm, born because of the following
	 *considerations:
	 *</p> 
	 *
	 *<p>
	 *The node which have great out-degree should be the starting points of the algorithm. They are called 
	 *<b>source nodes</b>. The nodes which have great in-degree are probably nodes with useful information
	 *for many clusters. They should be used but not further explored. They are called <b>terminal nodes</b>.
	 *a node can be both source and terminal.
	 *</p>
	 *
	 *<p>
	 *A node which is neither a source node nor a terminal node is called <p>accessory node</p>, and we
	 *assume it carries information which is of little relevance to the cluster, but that can be
	 *useful for some queries, so we include it.
	 *</p>
	 *
	 *<p>
	 *Note that a literal node not necessarily is the object of only one triple. It can be the object of various 
	 *triples. That means that the literals that are terminal nodes are very useful for the cluster. Moreover, 
	 *often a terminal node is an IRI which contains only an ID. To obtain useful information, it is necessary
	 *to include in the cluster other nodes around the terminal nodes that are literals and better describe its nature.
	 *</p>
	 *
	 *
	 * @param source the starting node to build the cloud
	 * @param tau the radius of the cloud
	 * @param connectivityList list of Strings representing the labels of the edges we will explore
	 * */
	public Model extendCluster(RDFNode source, int tau, List<String> connectivityList) {
		Resource sourceNode;
		if(source instanceof Resource) {
			sourceNode = (Resource) source;
		}
		else {
			//if source is in S, it should not be a literal but just in case
			System.err.println("The source node is not a subject resource. Strange?");
		}

		//create a new RDF model which is a cluster
		Model cluster = ModelFactory.createDefaultModel();
		//add the starting source
		cluster.createResource(source.toString());
		cluster = this.addAccessoryCloud(cluster, source);

		//queue to keep track of the nodes that has to be explore yet (the information kept
		//is the node and the radius tau that we still have)
		Queue<Pair<RDFNode, Integer>> extendingQueue = new LinkedList<Pair<RDFNode, Integer>>();
		extendingQueue.add(new MutablePair<RDFNode, Integer>(source, tau));

		while(! extendingQueue.isEmpty() ) {
			Pair<RDFNode, Integer> sPair = extendingQueue.remove();
			RDFNode s = sPair.getLeft();
			//mark the source node s as visited (thus the clustering algorithm will end)
			nodeColorMap.put(s, Color.black);
			Integer currentRadius = sPair.getRight();

			//check if it is a resource (it should be)
			Resource res = null;
			if(s instanceof Resource)
				res = (Resource) s;
			else
				throw new IllegalArgumentException("node is not a resource");
			//keep track of how far off we are from the origin (update the radius)
			int radius = sPair.getRight() - 1;

			cluster = addAccessoryCloud(cluster, res);
			StmtIterator resIterator = res.listProperties();
			while(resIterator.hasNext()) {
				Statement l = resIterator.next();
				RDFNode u = l.getObject();
				Color uColor = nodeColorMap.get(u);

				if((radius > 0) && (uColor != Color.black) && connectivityList.contains(l.getPredicate().toString())) {
					//if the edge is explorable, the radius is till greater than 0 and the node u has not already been
					//used as central node
					if(sourceSet.contains(u) && !terminalSet.contains(u)) {
						//enqueue the node and update its color
						Pair<RDFNode, Integer> uPair = new MutablePair<RDFNode, Integer>(u, radius);
						extendingQueue.add(uPair);
						nodeColorMap.put(u, Color.black);
						//add to the cluster
						cluster.add(l);
					}
				}
			}
		}
		return cluster;
	}

	/**Expands 1 cluster following my algorithm
	 * 
	 * @param source the starting node to build the cloud
	 * @param tau the radius of the cloud
	 * @param connectivityList list of Strings representing the labels of the edges we will explore
	 * */
	public Model extendCluster2(RDFNode source, int tau, List<String> connectivityList) {
		//first time we use this node as starting node
		nodeColorMap.put(source, Color.black);

		Resource sourceNode;
		if(source instanceof Resource) {
			sourceNode = (Resource) source;
		}
		else {
			//if source is in S, it should not be a literal but just in case
			System.err.println("The source node is not a subject resource. Strange?");
		}

		//create a new RDF model which is a cluster
		Model cluster = ModelFactory.createDefaultModel();
		//add the starting source
		cluster.createResource(source.toString());
		cluster = this.addAccessoryCloud(cluster, source);
		//queue to keep track of the nodes that has to be explore yet (the information kept
		//is the node and the radius tau that we still have)
		Queue<Pair<RDFNode, Integer>> extendingQueue = new LinkedList<Pair<RDFNode, Integer>>();
		extendingQueue.add(new MutablePair<RDFNode, Integer>(source, tau));
		while(! extendingQueue.isEmpty() ) {
			Pair<RDFNode, Integer> sPair = extendingQueue.remove();
			//take the node and treat it as a subject
			RDFNode s = sPair.getLeft();
			Integer currentRadius = sPair.getRight();
			//check if it is a resource (it should be)
			Resource res = null;
			if(s instanceof Resource)
				res = (Resource) s;
			else
				throw new IllegalArgumentException("node is not a resource");

			//selector to take all the triples with the source as subject
			Selector sel = new SimpleSelector((Resource) res, (Property) null, (RDFNode) null);
			//iterator over the outgoing edges of s
			StmtIterator jumpIterator = model.listStatements(sel);

			while(jumpIterator.hasNext()) {
				//check every element of the star which is not an accessory node
				Statement uStatement = jumpIterator.next();
				RDFNode u = uStatement.getObject();
				Property l = uStatement.getPredicate();
				Integer radius = currentRadius - 1;
				Color uColor = this.nodeColorMap.get(u);

				if(terminalSet.contains(u)) {
					//terminal node (which could also be a source node). We add the information and don't proceed
					cluster.add(uStatement);
					cluster = addAccessoryCloud(cluster, u);
					//color the node
					this.nodeColorMap.put(u, Color.gray);
					continue;
				}

				if((radius > 0) && (uColor != Color.black) && connectivityList.contains(l.toString())) {
					//if we want to traverse the edge (s, u)
					//conditions
					boolean one = this.terminalSet.contains(u);
					boolean two = this.sourceSet.contains(u);
					if(!one && two) {
						//a new source node which is not terminal
						cluster.add(uStatement);
						cluster = addAccessoryCloud(cluster, u);
						Pair<RDFNode, Integer> uPair = new MutablePair<RDFNode, Integer>(u, radius);
						extendingQueue.add(uPair);
						nodeColorMap.put(u, Color.black);

						//count the utilizations
						Integer value = this.nodeVisitMap.get(u);
						if(value == null) {
							nodeVisitMap.put(u, 1);
						}
						else {
							nodeVisitMap.put(u, value + 1);
						}
					}
				}//end if
			}
		}//end while on the queue
		return cluster;
	}

	/**Adds to the cluster represented by the argument Model the cloud
	 * of accessory nodes around the source node provided following the
	 * specifications of my (old) algorithm.
	 * 
	 * @param RDF model, the cluster to be populated with the cloud around the
	 * source node
	 * @param v the central node to be expanded with the accessory nodes (if they are present)
	 * */
	public Model addAccessoryCloudOld(Model cluster, RDFNode v) {
		//get the node in the whole graph
		Resource src = this.model.getResource(v.toString());
		StmtIterator srcIterator = src.listProperties();
		while(srcIterator.hasNext()) {
			//get the triple
			Statement t = srcIterator.next();
			//get the object of the triple
			RDFNode u = t.getObject();
			if(!sourceSet.contains(u) /*&& !terminalSet.contains(u)*/) {
				cluster.add(t);
				this.nodeColorMap.put(u, Color.gray);
				Integer value = this.nodeVisitMap.get(u);
				if(value == null) {
					nodeVisitMap.put(u, 1);
				}
				else {
					nodeVisitMap.put(u, value+1);
				}
			}
		}
		return cluster;
	}

	/**Adds to the cluster represented by the argument Model the cloud
	 * of accessory nodes around the source node provided following the
	 * specifications of my algorithm.
	 * 
	 * @param RDF model, the cluster to be populated with the cloud around the
	 * source node
	 * @param s the central node to be expanded with the accessory nodes (if they are present)
	 * */
	public Model addAccessoryCloud(Model cluster, RDFNode s) {
		//get the node in the whole graph
		Resource src = this.model.getResource(s.toString());
		//get all the outgoing neighbourhood
		StmtIterator srcIterator = src.listProperties();
		while(srcIterator.hasNext()) {//for each neighbour
			//get the triple
			Statement t = srcIterator.next();
			//get the object of the triple
			RDFNode u = t.getObject();
			if(!sourceSet.contains(u) && !terminalSet.contains(u)) {
				//accessory node (low degree) - this should
				//include the case with u literal
				cluster.add(t);
				this.addToNodeVisitMap(u);
			}
			else if(terminalSet.contains(u)) {
				//terminal node (can also be source, it's the same as long as it is terminal)
				cluster.add(t);
				this.addToNodeVisitMap(u);
				//if u is a Resource, we are interested in the literal labels that describes it
				if(u instanceof Resource) {
					Resource uRes = (Resource) u;
					StmtIterator uIter = uRes.listProperties();
					while(uIter.hasNext()) {
						Statement vT = uIter.next();
						RDFNode v = vT.getObject();
						//if v is an accessory node or a literal, we add it
						if(v.isLiteral() || (!sourceSet.contains(v) && !terminalSet.contains(v))) {
							cluster.add(vT);
						}
					}
				}
			}
		}
		return cluster;
	}


	/**Add +1 to the node u passed as parameter in the visit map
	 * */
	private void addToNodeVisitMap(RDFNode u) {
		Integer value = nodeVisitMap.get(u);
		if(value == null) {
			nodeVisitMap.put(u, 1);
		}
		else
			nodeVisitMap.put(u, value+1);
	}


	



	//getters and setters

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Set<RDFNode> getSourceSet() {
		return sourceSet;
	}

	public void setSourceSet(Set<RDFNode> sourceSet) {
		this.sourceSet = sourceSet;
	}

	public Set<RDFNode> getTerminalSet() {
		return terminalSet;
	}

	public void setTerminalSet(Set<RDFNode> terminalSet) {
		this.terminalSet = terminalSet;
	}

	public Map<RDFNode, Color> getNodeColorMap() {
		return nodeColorMap;
	}

	public void setNodeColorMap(Map<RDFNode, Color> nodeColorMap) {
		this.nodeColorMap = nodeColorMap;
	}

	public int getLambda_in() {
		return lambda_in;
	}

	public void setLambda_in(int lambda_in) {
		this.lambda_in = lambda_in;
	}

	public int getLambda_out() {
		return lambda_out;
	}

	public void setLambda_out(int lambda_out) {
		this.lambda_out = lambda_out;
	}
}
