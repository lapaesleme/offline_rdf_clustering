package it.unipd.dei.ims.clustering.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**This class contains all sorts of useful methods that can be used in the project. 
 * Because why not?
 * 
 * @author Dennis D.
 * */
public class UsefulMethods {

	public static final String NT = "N-TRIPLE";/** Rappresenta la stringa da dare come parametro ai metodi Jena
	 * per leggere e scrivere i file in formato .nt (triple)*/

	public final static Charset ENCODING = StandardCharsets.UTF_8;

	public final static String SUBJECT = "subject";

	public final static String PREDICATE = "predicate";

	public final static String OBJECT = "object";

	public final static String ID = "docno";


	/**Reads all the properties in all the propertie files
	 * in the directory /properties and returns a map that contains couples 
	 * <key, value>.
	 * */
	public static Map<String, String> getProperties() {
		//list all the properties file in the directory
		File folder = new File("properties");
		InputStream input;
		Properties prop = new Properties();

		Map<String, String> map = new HashMap<String, String>();

		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; ++i) {
			if(listOfFiles[i].isFile()) {
				try {
					//take the property file
					input = new FileInputStream(listOfFiles[i].getPath());
					prop.load(input);
					//take all the keys in the file
					Set<Object> keys = prop.keySet();
					for(Object k : keys) {
						String key = (String) k;
						String value = prop.getProperty(key);

						map.put(key, value);
					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		return map;
	}

	/**Method copied from StackOverflow. Orders the elements in a map by their values in 
	 * non increasing order. The elements are paths of file containing 1 integer
	 * in their name.
	 * */
	public static <K, V extends Comparable<? super V>> Map<K, V> 
	sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
		Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return -(o1.getValue()).compareTo( o2.getValue() );
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}


}
