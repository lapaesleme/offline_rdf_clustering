package it.unipd.dei.ims.tsa.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;

import it.unid.dei.ims.tsa.online.yosi.PriorGraphScoreComputationPhase;
import it.unid.dei.ims.tsa.online.yosi.TSAYosiAnswerRankingPhase;
import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.tsa.offline.ClusteringPhase;
import it.unipd.dei.ims.tsa.offline.ComputeTheTopKConnectivityList;
import it.unipd.dei.ims.tsa.offline.FromRDFGraphsToTRECDocuments;
import it.unipd.dei.ims.tsa.offline.IndexerDirectoryOfTRECFiles;
import it.unipd.dei.ims.tsa.offline.StatisticsComputationPhase;
import it.unipd.dei.ims.tsa.offline.VirtualDocumentCreationPhase;
import it.unipd.dei.ims.tsa.online.AnswerCompressionPhase;
import it.unipd.dei.ims.tsa.online.AnswerRankingPhase;
import it.unipd.dei.ims.tsa.online.CopyAndIndexSubgraphsPhase;
import it.unipd.dei.ims.tsa.online.FindBestGraphsPhase;
import it.unipd.dei.ims.tsa.online.SmartExplorationPhase;
import it.unipd.dei.ims.tsa.online.VDPWithBacktrackingPhase;
import it.unipd.dei.ims.tsa.online.VirtualDocAndPruningOnlinePhase;
import it.unipd.dei.ims.tsa.online.blanco.AnswerSubgraphsRankingPhase;
import it.unipd.dei.ims.tsa.online.blanco.RjDocumentCreationPhase;

public class TSAMainClass {

	/** Timer to control the execution of the single algorithms.
	 * */
	protected static Stopwatch singleMethodTimer;

	/** Timer to control the all around execution.
	 * */ 
	protected static Stopwatch allAroundTimer;

	/** Map of properties.
	 * */
	protected Map<String, String> map;


	boolean statisticsComputationFlag;
	boolean computeTopKConnectivityListFlag;
	boolean clusteringPhaseFlag;
	boolean fromClusterToTRECDocumentsFlag;
	boolean indexDirectoryOfTRECFilesFlag;
	boolean answerRankingFlag;
	boolean copyAndIndexFlag;
	boolean rjCollectionFlag;
	boolean rankAnswersFLag;
	boolean yosiRankingFlag;
	boolean offlinePhaseFlag;
	boolean onlineCommonPhaseFlag;
	boolean onlineBlancoPhaseFlag;
	boolean onlineYosiPhaseFlag;
	boolean virtualDocumentCreationFlag;
	boolean virtualDocAndPruningFlag;
	boolean onlineVirtualDocAndPruningFlag;
	boolean onlineVirtualDocAndPruningOnTheFlyFlag;
	boolean tsaVdpBackFlag;
	boolean tsaBackFlag;

	boolean mergingRequiredFlag;
	boolean indexMergedCollectionFlag;
	boolean findBestGraphsFlag;

	protected String query;
	protected String queryId;

	protected FileWriter fWriter;
	protected BufferedWriter timeWriter;

	/** The principal query directory where we operate. The subdirectories 
	 * represent the queries. */
	protected String mainQueryDirectory;

	/** Directory with the path of the algorithm we are dealing with*/
	protected String mainAlgorithmDirectory;

	protected String outputTimeFilePath;

	protected String jdbcConnectionString;

	protected boolean multipleQueries;

	protected String clusterDirectoryPath;

	/** the total degree of the whole collection. 
	 * Necessary in the Yosi part*/
	protected int totalDegree;

	/**The schema we are using right now*/
	protected String schema;

	protected String unigramIndexPath, bigramIndexPath;


	/** Builder for the class
	 * */
	public TSAMainClass() {

		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.setLabels();
	}

	protected void setup() throws IOException {
		this.mainAlgorithmDirectory = map.get("main.algorithm.directory");
		this.mainQueryDirectory = mainAlgorithmDirectory + "/queries";
		File f = new File(this.mainQueryDirectory);
		if(!f.exists()) {
			f.mkdirs();
		}

		this.multipleQueries = Boolean.parseBoolean(map.get("multiple.queries"));

		//create a writer which will write about the time used by the algorithm
		File d = new File(this.mainAlgorithmDirectory + "/log");
		if(!d.exists())
			d.mkdirs();
		this.outputTimeFilePath = this.mainAlgorithmDirectory + "/log/" + map.get("schema") + "_times.txt";
		this.fWriter = new FileWriter(this.outputTimeFilePath, true);
		this.timeWriter = new BufferedWriter(fWriter);
		timeWriter.write("\n---------- NEW EXECUTION ----------\n");

		singleMethodTimer = Stopwatch.createUnstarted();
		allAroundTimer = Stopwatch.createUnstarted();

		this.jdbcConnectionString = map.get("jdbc.connection.string");

		this.clusterDirectoryPath = map.get("cluster.directory.path");

		this.schema = map.get("schema");

		this.unigramIndexPath = map.get("unigram.index.path");
		this.bigramIndexPath = map.get("bigram.index.path");

	}



	protected void setLabels () {

		statisticsComputationFlag = Boolean.parseBoolean( map.get("statistics.computation.flag") );
		computeTopKConnectivityListFlag = Boolean.parseBoolean( map.get("compute.top.k.connectivity.list.flag") );
		clusteringPhaseFlag = Boolean.parseBoolean( map.get("clustering.phase.flag") );
		fromClusterToTRECDocumentsFlag = Boolean.parseBoolean( map.get("from.cluster.to.trec.documents.flag") );
		indexDirectoryOfTRECFilesFlag = Boolean.parseBoolean( map.get("index.directory.of.trec.files.flag") );
		copyAndIndexFlag =  Boolean.parseBoolean( map.get("copy.and.index.flag") );
		answerRankingFlag =  Boolean.parseBoolean( map.get("answer.ranking.flag") );
		rjCollectionFlag =  Boolean.parseBoolean( map.get("rj.collection.flag") );
		rankAnswersFLag =  Boolean.parseBoolean( map.get("rank.answers.flag") );
		yosiRankingFlag = Boolean.parseBoolean(map.get("yosi.ranking.flag"));
		mergingRequiredFlag = Boolean.parseBoolean(map.get("merging.required.flag"));
		indexMergedCollectionFlag = Boolean.parseBoolean(map.get("index.merged.collection.flag"));
		findBestGraphsFlag = Boolean.parseBoolean(map.get("find.best.graphs.flag"));
		virtualDocumentCreationFlag = Boolean.parseBoolean(map.get("virtual.documents.creation.flag"));
		virtualDocAndPruningFlag = Boolean.parseBoolean(map.get("virtual.doc.and.pruning.flag"));

		offlinePhaseFlag = Boolean.parseBoolean(map.get("offline.phase.flag"));
		onlineCommonPhaseFlag = Boolean.parseBoolean(map.get("online.common.phase.flag"));
		onlineBlancoPhaseFlag = Boolean.parseBoolean(map.get("online.blanco.phase.flag"));
		onlineYosiPhaseFlag = Boolean.parseBoolean(map.get("online.yosi.phase.flag"));
		onlineVirtualDocAndPruningFlag = Boolean.parseBoolean(map.get("online.virtual.doc.and.pruning.flag"));
		onlineVirtualDocAndPruningOnTheFlyFlag = Boolean.parseBoolean(map.get("online.virtual.doc.and.pruning.on.the.fly.flag"));
		tsaVdpBackFlag = Boolean.parseBoolean(map.get("tsa.vdp.back.flag"));
		tsaBackFlag = Boolean.parseBoolean(map.get("tsa.back.flag"));
	}

	public void executeTSAAlgorithm() throws IOException {
		this.executeTSAAlgorithm(false);
	}

	protected int getTotalDegreeForYosi() {
		String SQL_GET_TOTAL_DEGREE = "SELECT total_graph_degree from " +  this.schema + ".metadata";
		PreparedStatement totalDegreeStatement;
		int totalDegree = 0;
		Connection connection = null;
		try {
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());

			totalDegreeStatement = connection.prepareStatement(SQL_GET_TOTAL_DEGREE);
			ResultSet rs = totalDegreeStatement.executeQuery();
			if(rs.next())
				totalDegree = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return totalDegree;
	}

	/** Reads a list of queries from a property files 
	 * and executes each one of them.
	 * */
	public void executeTSAAlgorithmMultipleTimes() {
		try {
			//get the list of query
			String queryPath = map.get("query.file");
			Map<String, String> queryMap = PropertiesUsefulMethods.getSinglePropertyFileMap(queryPath);

			//setup the connection
			ConnectionHandler.createConnectionAsOwner(map.get("jdbc.connection.string"), this.getClass().getName());

			this.totalDegree = this.getTotalDegreeForYosi();

			for(Entry<String, String> entry : queryMap.entrySet()) {
				//id of the query
				String queryId = entry.getKey();
				String query = entry.getValue();

				//prepare the execution
				this.setQueryId(queryId);
				this.setQuery(query);

				this.executeTSAAlgorithm(true);

			}
			timeWriter.close();
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public void executeTSAAlgorithm(boolean multipleQueries) throws IOException {

		allAroundTimer.start();
		singleMethodTimer = Stopwatch.createUnstarted();


		//********** offline phase **********
		if(offlinePhaseFlag) {
			this.offlinePhase();

		}

		// ********** ONLINE PHASE - IR **********

		if(this.onlineCommonPhaseFlag) {
			System.out.print("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the Blanco ranking)\n"
					+ "keyword query: \n" + query + "\n\n");

			timeWriter.write("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the simple IR part)\n"
					+ "keyword query: \n" + query + "\n\n");

			this.onlineIRPhase(multipleQueries);	

		}

		// ********** ONLINE PHASE: BLANCO **********
		if(this.onlineBlancoPhaseFlag) {

			System.out.print("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the Blanco ranking)\n"
					+ "keyword query: \n" + query + "\n\n");

			timeWriter.write("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the Blanco ranking)\n"
					+ "keyword query: \n" + query + "\n\n");

			long start = System.currentTimeMillis();
			this.onlinePhaseBlanco(multipleQueries);
			long last = System.currentTimeMillis() - start;
			System.out.println("time in milliseconds: " + last);
		}

		// ********** ONLINE PHASE: YOSI ***********

		if(this.onlineYosiPhaseFlag) {
			System.out.print("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the Yosi ranking)\n"
					+ "keyword query: \n" + query + "\n\n");

			timeWriter.write("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the Yosi ranking)\n"
					+ "keyword query: \n" + query + "\n\n");
			long start = System.currentTimeMillis();
			this.onlinePhaseYosi(multipleQueries);
			long end = System.currentTimeMillis() - start;
			System.out.println("time of Yosi in milliseconds: " + end);
		}

		// ****** online phase PRUNING (VDP) ********* 

		if(this.onlineVirtualDocAndPruningFlag) {
			System.out.print("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the Pruning algorithm ranking)\n"
					+ "keyword query: \n" + query + "\n\n");

			timeWriter.write("\n***********\n\nExecuting now the on-line part of query " + this.queryId + ""
					+ " (the Pruning algorithm ranking)\n"
					+ "keyword query: \n" + query + "\n\n");

			long start = System.currentTimeMillis();
			this.onlinePhasePruning(multipleQueries);
			long end = System.currentTimeMillis() - start;
			System.out.println("time of Pruning in milliseconds: " + end);
		}


		timeWriter.write("the query " + queryId + " complexively  "
				+ "required: " + allAroundTimer);
		System.out.println("the query " + queryId + " complexively  "
				+ "required: " + allAroundTimer.stop());
		timeWriter.flush();
		allAroundTimer.reset();
	}

	/** Execution of the offline phase of the algorithm.
	 * */
	protected void offlinePhase() {

		if(ThreadState.isOffLine())
			this.phase1();
		if(ThreadState.isOffLine())
			this.phase2and3();//creation of "clusters"
		if(ThreadState.isOffLine())
			this.phase4();
		if(ThreadState.isOffLine())
			this.phase5();

	}

	protected void onlineIRPhase(boolean multipleQueries) throws IOException {
		//old versions
		//		this.phase6(multipleQueries);
		//		this.phase7(multipleQueries);

		//performing the IR phase with merging
		this.phase6and7bis(multipleQueries);

		//		this.phase7ter(multipleQueries);
	}

	protected void phase1() {
		if(statisticsComputationFlag) {
			//phase 1: find the statistics
			if(singleMethodTimer.isRunning()) {
				singleMethodTimer.stop();
				singleMethodTimer.reset();
			}
			Stopwatch timer = Stopwatch.createStarted();

			StatisticsComputationPhase phase1 = new StatisticsComputationPhase();
			phase1.computeStatisticsUsingOnlyRDB();

			System.out.println("phase 1 ended in: " + timer.stop());
			try {
				timeWriter.write("phase 1 ended in: " + timer);
				timeWriter.newLine();
				timeWriter.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			timer.reset();
		}
	}


	/** Clustering.
	 * */
	protected void phase2and3 () {
		List<String> connectivityList = null;
		if(clusteringPhaseFlag) {
			//phase 2: compute the connectivity list (list of edges we can traverse)
			Stopwatch timer = Stopwatch.createStarted();

			ComputeTheTopKConnectivityList phase2 = new ComputeTheTopKConnectivityList();
			connectivityList = phase2.getTopKConnectivityList();

			ClusteringPhase phase3 = new ClusteringPhase();
			phase3.setClusterDirectoryPath(this.mainAlgorithmDirectory + "/clusters");

			phase3.TSAAlgorithm(connectivityList);

			System.out.println("phase 2 and 3 ended in: " + timer.stop());
			try {
				timeWriter.write("phases 2 and 3 ended in: " + timer);
				timeWriter.newLine();
				timeWriter.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			timer.reset();
		}
	}

	@Deprecated
	protected void phase3bis() {
		if(virtualDocumentCreationFlag) {
			//phase 3 bis: creation of virtual documents
			Stopwatch timer = Stopwatch.createStarted();

			//being an off-line phase, all parameters are read
			//in the builder from properties file
			VirtualDocumentCreationPhase phase3b = new VirtualDocumentCreationPhase();
			phase3b.createVirtualDocuments();
			phase3b.convertAndIndex();

			System.out.println("phase 3 bis (virtual docs creation) ended in: " + timer.stop());
			timer.reset();
		}
	}


	protected void phase4 () {
		if(fromClusterToTRECDocumentsFlag) {
			//phase 4: convert from RDF to TREC
			Stopwatch timer = Stopwatch.createStarted();

			FromRDFGraphsToTRECDocuments phase4 = new FromRDFGraphsToTRECDocuments();
			//set the directory where to find the rdf clusters
			phase4.setGraphsMainDirectory(this.mainAlgorithmDirectory + "/clusters");
			//set the directory where to write the TREC files
			phase4.setOutputDirectory(this.mainAlgorithmDirectory + "/cluster_collection");

			phase4.convertRDFGraphsInTRECDocuments();

			System.out.println("phase 4 ended in: " + timer.stop());
			try {
				timeWriter.write("phase 4 ended in: " + timer);
				timeWriter.newLine();
				timeWriter.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			timer.reset();
		}
	}

	protected void phase5 () {
		if(indexDirectoryOfTRECFilesFlag) {
			//phase 5: create the index
			Stopwatch timer = Stopwatch.createStarted();

			//we need two indexes, one of unigrams and the second one of bigrams
			IndexerDirectoryOfTRECFiles phase5 = new IndexerDirectoryOfTRECFiles();

			phase5.setDirectoryToIndex(this.mainAlgorithmDirectory + "/cluster_collection");
			phase5.setIndexPath(this.mainAlgorithmDirectory + "/cluster_index");
			if(ThreadState.isOffLine())
				phase5.index("unigram");

			phase5.setIndexPath(this.mainAlgorithmDirectory + "/bigram_cluster_index");
			if(ThreadState.isOffLine())
				phase5.index("bigram");

			System.out.println("phase 5 ended in: " + timer.stop());
			try {
				timeWriter.write("phase 5 ended in: " + timer);
				timeWriter.newLine();
				timeWriter.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			timer.reset();
		}
	}


	protected void phase6(boolean multipleQueries) throws IOException {
		if(answerRankingFlag) {
			System.out.println("begin phase 6 with query " + queryId + ": " + query);
			//phase 6: create the first ranking of the graphs with 'plain' IR
			Stopwatch timer = Stopwatch.createStarted();

			//execute a first ranking in IR, writes the answer in a TS+IR.res file
			AnswerRankingPhase phase6 = new AnswerRankingPhase();
			if(multipleQueries) {
				phase6.setQuery(query);
				phase6.setResultDirectory(mainQueryDirectory + "/" + queryId + "/IR");
				phase6.setQueryId(queryId);
			}
			phase6.subgraphsRankingPhase();

			System.out.println("phase 6 ended in: " + timer.stop());

			timeWriter.write("phase 6 ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	/** Old phase 7. This phase uses a .rank
	 * file to create a directory with the 
	 * graphs that can be found in the file. THen it
	 * creates the corresponding TREC documents and the index.
	 * */
	protected void phase7(boolean multipleQueries) throws IOException {
		if(copyAndIndexFlag) {
			//phase 7: index the candidate collection
			System.out.println("begin phase 7 with query " + queryId + ": " + query);
			Stopwatch timer = Stopwatch.createStarted();

			CopyAndIndexSubgraphsPhase phase7 = new CopyAndIndexSubgraphsPhase();
			if(multipleQueries) {
				phase7.setQueryDirectoryPath(mainQueryDirectory + "/" + queryId);
				phase7.setResourceFilePath(mainQueryDirectory + "/" + queryId + "/IR/IR_rank.txt");
				phase7.setOutputCandidateGraphsDirectoryPath(mainQueryDirectory + "/"+ queryId + "/candidate_graphs");
				phase7.setOutputCandidateCollectionDirectoryPath(mainQueryDirectory + "/"+ queryId + "/candidate_collection");
				phase7.setOutputCollectionIndexPath(mainQueryDirectory + "/" + queryId + "/candidate_collection_index");
			}
			phase7.createCandidateCollection();

			timeWriter.write("phase 7 ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();

			System.out.println("phase 7 ended in: " + timer.stop());
		}
	}

	/** In this alternative phase to 6 and 7,
	 * we combine different operations.
	 * <p>
	 * First, we rank the clusters and create a file .res obtained
	 * by the BM25 model.
	 * <p>
	 * Then we look at these graphs and create a collection
	 * of maximum 1000 new graphs obtained with a merging algorithm.
	 * The merging algorithm simply scans the list of graphs and when a graph
	 * is partially overlapped with the following graphs,
	 * we merge the two.
	 * <p>
	 *  We create in this way a new collection of graphs, 
	 *  then we convert them in TREC documents and index them.
	 *  <p>
	 *  Finally, we use this new collection to create a new ranking with BM25.
	 *  This second ranking is the output of our first pipeline.
	 * @throws IOException 
	 * */
	protected void phase6and7bis(boolean multipleQueries) throws IOException {
		if(indexMergedCollectionFlag) {
			System.out.println("begin phase 6 with query " + queryId + ": " + query);
			//phase 6: create the first ranking of the graphs with 'plain' IR
			if(singleMethodTimer.isRunning())
				singleMethodTimer.stop();
			Stopwatch timer = Stopwatch.createStarted();

			//execute a first ranking in IR, writes the answer in a TS+IR.res file
			AnswerRankingPhase phase6 = new AnswerRankingPhase();
			phase6.setQuery(query);
			phase6.setQueryId(queryId);
			phase6.setResultDirectory(mainQueryDirectory + "/" + queryId + "/BM25");
			phase6.setIndexPath(this.mainAlgorithmDirectory + "/cluster_index");

			if(DatabaseState.getState() == DatabaseState.LUBM) {
				/*in the case the database is LUBM, we add a module
				 * we only keep the graphs that
				 * contain all the keywords. We then rank them with BM25
				 * */
				phase6.setFilteredGraphsDirectory(this.mainQueryDirectory + "/" + queryId + "/BM25/filteredGraphs" );
				phase6.setFilteredCollectionDirectory(this.mainQueryDirectory + "/" + queryId + "/BM25/filteredCollection");
				phase6.setFilteredCollectionIndexDirectory(this.mainQueryDirectory + "/" + queryId + "/BM25/filteredCollectionIndex");
				phase6.setGraphClustersDir(this.mainAlgorithmDirectory + "/clusters");

				try { 
					phase6.rankTheGraphsWithOnlyFullGraphs();
				} catch(NullPointerException e) {
					/*XXX in this case, the algorithm found no graph
					//with all the query words (strange but possible with LUBM)
					 * so we return an exception and we catch it in order to deal 
					 * with this fact */
					//e.printStackTrace();
					throw new NullPointerException(e.getMessage());
				}

			} else if (DatabaseState.getState() == DatabaseState.DEFAULT) {

				phase6.setGraphClustersDir(this.mainAlgorithmDirectory + "/clusters");
				if(ThreadState.isOnLine())
					phase6.subgraphsRankingPhase();
			} else {
				phase6.setGraphClustersDir(this.mainAlgorithmDirectory + "/clusters");
				if(ThreadState.isOnLine())
					phase6.subgraphsRankingPhase();
			}


			//create the merged collection
			AnswerCompressionPhase phase61 = new AnswerCompressionPhase();
			phase61.setRankingFilePath(mainQueryDirectory + "/" + queryId + "/BM25/run.txt");
			phase61.setCompressedCollectionOutputDirectoryPath(mainQueryDirectory + "/" + queryId + "/BM25/merged_graphs");
			if(multipleQueries) {
				phase61.setOriginalCollectionDirectoryPath(this.mainAlgorithmDirectory + "/clusters"); 
			}
			if(ThreadState.isOnLine())
				phase61.compressTheCollectionWithThreshold();				

			//convert the new collection of graphs in documents and index them
			FromRDFGraphsToTRECDocuments phase7bis = new FromRDFGraphsToTRECDocuments();

			if(multipleQueries) {
				phase7bis.setGraphsMainDirectory(mainQueryDirectory + "/" + queryId + "/BM25/merged_graphs"); 
				phase7bis.setOutputDirectory(mainQueryDirectory + "/" + queryId + "/BM25/merged_collection");
			}
			if(ThreadState.isOnLine())
				phase7bis.convertRDFGraphsInTRECDocuments();

			//now we index them - we perform the double indexing because
			//it will be useful for other methods and it is quick
			IndexerDirectoryOfTRECFiles phase72bis = new IndexerDirectoryOfTRECFiles();
			if(multipleQueries) {
				phase72bis.setDirectoryToIndex(mainQueryDirectory + "/" + queryId + "/BM25/merged_collection");
				phase72bis.setIndexPath(mainQueryDirectory + "/" + queryId + "/BM25/merged_unigram_index");
			}
			if(ThreadState.isOnLine())
				phase72bis.index("unigram");
			if(multipleQueries) {
				phase72bis.setIndexPath(mainQueryDirectory + "/" + queryId + "/BM25/merged_bigram_index");
			}
			if(ThreadState.isOnLine())
				phase72bis.index("bigram");

			//rank the new collection
			if(multipleQueries) {
				phase6.setQuery(query);
				phase6.setIndexPath(mainQueryDirectory + "/" + queryId + "/BM25/merged_unigram_index");
				phase6.setResultDirectory(mainQueryDirectory + "/" + queryId + "/BM25/answer");
				phase6.setQueryId(queryId);
			}
			if(ThreadState.isOnLine())
				phase6.subgraphsRankingPhase();

			System.out.println("TSA+BM25 phase ended in: " + timer.stop());

			timeWriter.write("TSA+BM25 phase ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	/** NEW phase, introduce to improve the online fast algorithm.
	 * For now, it only filters the graphs of the merged collection,
	 * keeping only the ones that have all the query words.
	 * The collection was called ultimate collection
	 * <p>
	 * @deprecated rejected method due to poor performances over IMDB (LinkedMDB a little better, 
	 * but who cares)
	 * */
	@Deprecated
	protected void phase7ter(boolean multipleQueries) {
		if(findBestGraphsFlag) {
			FindBestGraphsPhase phase = new FindBestGraphsPhase();

			if(multipleQueries) {
				phase.setQuery(query);
				phase.setCollectionIndexDirectory(mainQueryDirectory + "/" + queryId + "/IR/merged_unigram_index" );
				phase.setCollectionGraphDirectory(mainQueryDirectory + "/" + queryId + "/IR/merged_graphs");
				phase.setNewCollectionPath(mainQueryDirectory + "/" + queryId + "/IR/ultimate_graphs");
			}

			try {
				List<String> list = phase.findBestGraphs();
				phase.createNewCollectionCopyingFromListOfDocno(list);

				//now we can rank the ultimate collection
				AnswerRankingPhase ranking = new AnswerRankingPhase();

				if(multipleQueries) {
					ranking.setQuery(query);
					ranking.setIndexPath(mainQueryDirectory + "/" + queryId + "/IR/ultimate_index");
					ranking.setResultDirectory(mainQueryDirectory + "/" + queryId + "/IR/answer");
					ranking.setAnswerFileName("ultimate_run.txt");
					ranking.setQueryId(queryId);
				}
				ranking.subgraphsRankingPhase();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	protected void onlinePhaseBlanco(boolean multipleQueries) throws IOException {
		this.phase1Blanco(multipleQueries);
		this.phase2Blanco(multipleQueries);
	}

	protected void onlinePhaseYosi(boolean multipleQueries) throws IOException {
		try {
			this.phase1YosiBis(multipleQueries);
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
	}

	protected void onlinePhasePruning(boolean multipleQueries) {
		try {
			//			this.phase11(multipleQueries);
			this.phase11bis(multipleQueries);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void phase1Blanco(boolean multipleTimes) throws IOException {
		if(rjCollectionFlag) {
			//phase 8: create the supporting documents R_j
			System.out.println("phase 8 (Blanco) started: creation of the R_j documents");
			Stopwatch timer = Stopwatch.createStarted();

			RjDocumentCreationPhase phase8 = new RjDocumentCreationPhase();
			if(multipleTimes) {
				//				phase8.setAnswerSubgraphsDirectory(mainQueryDirectory + "/" + queryId + "/candidate_graphs");
				//				phase8.setRjOutputDirectory(mainQueryDirectory + "/" + queryId + "/BLANCO/rj");

				phase8.setAnswerSubgraphsDirectory(mainQueryDirectory + "/" + queryId + "/IR/merged_graphs");
				phase8.setRjOutputDirectory(mainQueryDirectory + "/" + queryId + "/BLANCO/rj");

			}
			phase8.createTheRjFiles();

			System.out.println("phase 8 ended in: " + timer.stop());

			timeWriter.write("phase 8 (Blanco) ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();

		}
	}

	protected void phase2Blanco(boolean multipleTimes) throws IOException {
		if(rankAnswersFLag) {
			//phase 9: create the supporting documents R_j
			System.out.println("phase 9 (Blanco) started: ranking the documents");
			Stopwatch timer = Stopwatch.createStarted();

			AnswerSubgraphsRankingPhase phase9 = new AnswerSubgraphsRankingPhase();
			if(multipleTimes) {
				phase9.setQuery(query);
				phase9.setResultDirectory(mainQueryDirectory + "/" + queryId + "/BLANCO/rank");
				//				phase9.setIndexPath(mainQueryDirectory + "/" + queryId + "/candidate_collection_index"); 
				phase9.setIndexPath(mainQueryDirectory + "/" + queryId + "/IR/merged_unigram_index"); 

				//set properties for the Blanco-Elbassuoni Language Model 
				System.setProperty("blanco.rj.indexes.directory", mainQueryDirectory + "/" + queryId + "/BLANCO/rj_indexes");
				System.setProperty("blanco.rj.index.directory", mainQueryDirectory + "/" + queryId + "/BLANCO/rj_index");
				System.setProperty("blanco.r.j.output.directory", mainQueryDirectory + "/" + queryId + "/BLANCO/rj");

				System.setProperty("blanco.subgraphs.directory", mainQueryDirectory + "/" + queryId + "/IR/merged_graphs");
				System.setProperty("answer.index.directory", mainQueryDirectory + "/" + queryId + "/IR/merged_unigram_index");
			}
			phase9.subgraphsRankingPhase();

			System.out.println("phase 9 ended in: " + timer.stop());

			timeWriter.write("phase 9 (Blanco) ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	protected void phase1Yosi(boolean multipleQueries) throws IOException {
		if(yosiRankingFlag) {
			//need to create a bigram index of the candidate answers
			System.out.println("Start phase 10: keyword nodes ranking");
			Stopwatch timer = Stopwatch.createStarted();

			IndexerDirectoryOfTRECFiles indexer = new IndexerDirectoryOfTRECFiles();
			TSAYosiAnswerRankingPhase phase10 = new TSAYosiAnswerRankingPhase();

			if(multipleQueries) {
				//first phase: index the bigrams
				indexer.setDirectoryToIndex(this.mainQueryDirectory + "/" + queryId + "/candidate_collection");
				indexer.setIndexPath(this.mainQueryDirectory + "/" + queryId + "/candidate_collection_bigram_index");
				indexer.index("bigram");

				indexer.setIndexPath(this.mainQueryDirectory + "/" + queryId + "/candidate_collection_unigram_index");
				indexer.index("unigram");

				phase10.setQuery(query);
				phase10.setUnigramIndexPath(this.mainQueryDirectory + "/" + queryId + "/candidate_collection_unigram_index");
				phase10.setBigramIndexPath(this.mainQueryDirectory + "/" + queryId + "/candidate_collection_bigram_index");
				phase10.setAnswerRankOutputFilePath(this.mainQueryDirectory + "/" + queryId + "/YOSI/rank/rank.txt");

				phase10.initialize();
			}
			phase10.rankTheAnswers();

			System.out.println("phase 10 ended in: " + timer.stop());

			timeWriter.write("phase 10 (Yosi ranking) ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();

			singleMethodTimer.reset();
		}
	}

	protected void phase1YosiBis(boolean multipleQueries) throws IOException, RDFParseException, RDFHandlerException {
		if(yosiRankingFlag) {
			//need to create a bigram index of the candidate answers
			System.out.println("Start phase 10: keyword nodes ranking");
			Stopwatch timer = Stopwatch.createStarted();

			//first we create the static scores for the answer graphs of this query
			PriorGraphScoreComputationPhase preComputation = new PriorGraphScoreComputationPhase();
			if(multipleQueries) {
				preComputation.setGraphToScoreDirectoryPath(this.mainQueryDirectory + "/" + queryId + "/IR/merged_graphs");
				preComputation.setPriorScoresOutputFilePath(this.mainQueryDirectory + "/" + queryId + "/YOSI/static_scores.xml");
				preComputation.setTotalDegree(totalDegree);
			}
			preComputation.computePriorForTheGraphs();

			TSAYosiAnswerRankingPhase phase10 = new TSAYosiAnswerRankingPhase();

			if(multipleQueries) {
				phase10.setQuery(query);
				phase10.setUnigramIndexPath(this.mainQueryDirectory + "/" + queryId + "/IR/merged_unigram_index");
				phase10.setBigramIndexPath(this.mainQueryDirectory + "/" + queryId + "/IR/merged_bigram_index");
				phase10.setAnswerRankOutputFilePath(this.mainQueryDirectory + "/" + queryId + "/YOSI/rank/run_without_prior.txt");
				phase10.setTotalDegree(totalDegree);
				phase10.setStaticScoreFilePath(this.mainQueryDirectory + "/" + queryId + "/YOSI/static_scores.xml");

				phase10.initialize();
			}
			phase10.rankTheAnswers();

			System.out.println("phase 10 ended in: " + timer.stop());

			timeWriter.write("phase 10 (Yosi ranking) ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();

			singleMethodTimer.reset();
		}
	}

	@Deprecated
	protected void phase11(boolean multipleQueries) throws IOException {
		if(this.virtualDocAndPruningFlag) {
			System.out.println("Start phase 11: pruning algorithm ranking");
			Stopwatch timer = Stopwatch.createStarted();

			SmartExplorationPhase phase11 = new SmartExplorationPhase();
			if(multipleQueries) {
				phase11.setRunFile(this.mainQueryDirectory + "/" +this.queryId + "/IR/answer/run.txt");
				phase11.setAnswerGraphsDirectory(this.mainQueryDirectory + "/" + this.queryId + "/IR/merged_graphs");
				//jdbcConnectionString read from main.properties
				//indexes directories paths read from main.properties
				//directory with the virtual graphs read from main.properties
				phase11.setGraphsOutputDirectory(this.mainQueryDirectory + "/" + this.queryId + "/PRUNING/answer_graphs");
				phase11.setQueryId(Integer.parseInt(this.queryId));
				phase11.setQuery(this.query);
				phase11.setResultOutputFile(this.mainQueryDirectory + "/" + this.queryId + "/PRUNING/answer/run.txt");
			}

			//XXX very important (otherwise you will get null answers)
			//execute this method once if it's the first time
			//phase11.setupTheSystem();

			phase11.smartTraversing();



			System.out.println("phase 11 ended in: " + timer.stop());

			timeWriter.write("phase 11 (PRUNE ranking) ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();

			singleMethodTimer.reset();
		}
	}


	/** Executes the TSA+VDP+BACK algorithm
	 * 
	 * TODO prepare the same for TSA+BACK
	 * */
	protected void tsaVdpBackPhase() {
		System.out.println("Executing TSA+VDP+BACK algorithm for query " + this.queryId);

		VDPWithBacktrackingPhase execution = new VDPWithBacktrackingPhase();

		execution.setRunFile(this.mainQueryDirectory + "/" + this.queryId + "/BM25/answer/run.txt");
		execution.setAnswerGraphOutputDirectory(this.mainQueryDirectory + "/" + this.queryId + "/TSA_VDP_BACK/answer_graphs");
		execution.setResultOutputFile(this.mainQueryDirectory + "/" + this.queryId + "/TSA_VDP_BACK/answer/run.txt");
		execution.setReadingGraphsDirectory(this.mainQueryDirectory + "/" + this.queryId + "/BM25/merged_graphs");
		
		execution.setMethod("TSA+VDP+BACK");
		execution.setQuery(this.query);
		execution.setQueryId(queryId);
		
		execution.tsa_vdp_backward();

	}


	protected void tsaBackPhase() {
		System.out.println("Executing TSA+BACK algorithm");

		VDPWithBacktrackingPhase execution = new VDPWithBacktrackingPhase();

		execution.setRunFile(this.mainQueryDirectory + "/" + this.queryId + "/BM25/answer/run.txt");
		execution.setAnswerGraphOutputDirectory(this.mainQueryDirectory + "/" + this.queryId + "/TSA_BACK/answer_graphs");
		execution.setResultOutputFile(this.mainQueryDirectory + "/" + this.queryId + "/TSA_BACK/answer/run.txt");
		execution.setReadingGraphsDirectory(this.mainQueryDirectory + "/" + this.queryId + "/BM25/merged_graphs");
		
		execution.setMethod("TSA+BACK");
		execution.setQuery(this.query);
		execution.setQueryId(queryId);
		
		execution.tsa_backtrack();
		
	}

	/** New phase performing the virtual document creation on the fly.
	 * NB: this phase must be preceded by the merging phase
	 * and ranking, because it uses those result file.
	 * 
	 * This is the TSA+VDP algorithm,  boy (VDP part, after the merging - 
	 * I know, it's complicated, but I was not the most experienced programmer, you know)
	 * */
	protected void phase11bis(boolean multipleQueries) throws IOException, 
	IllegalStateException {
		if(onlineVirtualDocAndPruningOnTheFlyFlag) {
			System.out.println("Start phase 11bis: pruning on the fly algorithm ranking");

			if(singleMethodTimer.isRunning())
				singleMethodTimer.stop();
			Stopwatch timer = Stopwatch.createStarted();

			VirtualDocAndPruningOnlinePhase phase11bis = new VirtualDocAndPruningOnlinePhase();
			if(multipleQueries) {
				//tau is set from main.properties
				phase11bis.setBeforePruningAnswerGraphsDirectory(this.mainQueryDirectory + "/" + this.queryId + "/VDP/before_pruning_graphs");
				phase11bis.setRunFile(this.mainQueryDirectory + "/" + this.queryId + "/BM25/answer/run.txt");

				//limit read from main.properties
				phase11bis.setUnigramIndexPath(this.mainAlgorithmDirectory + "/cluster_index");
				phase11bis.setBigramIndexPath(this.mainAlgorithmDirectory + "/bigram_cluster_index");

				phase11bis.setAnswerGraphOutputDirectory(this.mainQueryDirectory + "/" + this.queryId + "/VDP/pruning_answer_graphs");
				phase11bis.setResultOutputFile(this.mainQueryDirectory + "/" + this.queryId + "/VDP/answer/pruning_run.txt");
				phase11bis.setQuery(this.query);
				phase11bis.setQueryId(this.queryId);
				phase11bis.setReadingGraphsDirectory(this.mainQueryDirectory + "/" + this.queryId + "/BM25/merged_graphs");
			}

			//I called it smart traversing but it is
			//the ranking algorithm
			try {
				if(ThreadState.isOnLine())
					phase11bis.smartTraversing();
			} catch (IllegalArgumentException e) {
				//there is no graph in the collection, so we cannot do anything
				throw new IllegalStateException(e.getMessage());
			}


			System.out.println("phase 11 bis (TSA+VDP) ended in: " + timer.stop());

			timeWriter.write("phase 11 (TSA+VDP) ended in: " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}


	/** Test main (if I rename it 'test' is for debugging reasons)
	 * */
	public static void test(String[] args) throws IOException {
		TSAMainClass execution = new TSAMainClass();
		if(execution.isMultipleQueries()) {
			execution.executeTSAAlgorithmMultipleTimes();
		}
		else
			execution.executeTSAAlgorithm();
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public boolean isMultipleQueries() {
		return multipleQueries;
	}

	public void setMultipleQueries(boolean multipleQueries) {
		this.multipleQueries = multipleQueries;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

}
