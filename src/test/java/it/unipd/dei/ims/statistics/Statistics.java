package it.unipd.dei.ims.statistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import it.unipd.dei.ims.clustering.utilities.UsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.UsefulConstants;

/**Object representing some useful statistics about the RDF graph.
 *<p>
 *This is an unefficient class: it keeps everything in memory and uses the library Jena. 
 *For better performance see {@link BlazegraphStatisticsWithDatabaseSupport}.
 * */
@Deprecated
public class Statistics {

	/**The cardinality of E, |E|*/
	private int edgeSetCardinality;
	/**Keeps the record c(l, E) for every label l*/
	private Map<String, Integer> labelCounterMap;
	/**Map containing the numerator of the ACD(l) for each label l appearing in the graph*/
	private Map<String, Integer> labelAverageDegreeMap;
	/**Contains the ACD for each label l*/
	private Map<String, Double> averageCombinedDegreeMap;
	/**Stores the beta score for each label*/
	private Map<String, Double> betaScoreMap;
	/**Contains the f-measure between the ACD and the frequency for each label l*/
	private Map<String, Double> fMeasureMap;

	/**Keeps track of the in degree of the various nodes in the RDF model*/
	private Map<RDFNode, Integer> inDegreeMap;
	/**Keeps track of the out degree of the various nodes in the RDF model*/
	private Map<RDFNode, Integer> outDegreeMap;

	/**the two beta parameters composing the beta-score function*/ 
	private double beta1 = 1;
	private double beta2 = 1;

	/**the graph to whom the Statistic class is associated*/
	private Model graph;

	//constructors
	public Statistics () {
		this.edgeSetCardinality = 0;
		this.labelCounterMap = new HashMap<String, Integer>();
		this.labelAverageDegreeMap = new HashMap<String, Integer>();
		this.inDegreeMap = new HashMap<RDFNode, Integer>();
		this.outDegreeMap = new HashMap<RDFNode, Integer>();
		this.betaScoreMap = new HashMap<String, Double>();
		this.averageCombinedDegreeMap = new HashMap<String, Double>();
		this.fMeasureMap = new HashMap<String, Double>();
		this.graph = null;
	}

	public Statistics (Model m) {
		this.edgeSetCardinality = 0;
		this.labelCounterMap = new HashMap<String, Integer>(22000000);
		this.labelAverageDegreeMap = new HashMap<String, Integer>();
		this.betaScoreMap = new HashMap<String, Double>();
		this.inDegreeMap = new HashMap<RDFNode, Integer>();
		this.outDegreeMap = new HashMap<RDFNode, Integer>();
		this.averageCombinedDegreeMap = new HashMap<String, Double>();
		this.fMeasureMap = new HashMap<String, Double>();
		this.graph = m;
	}

	public Statistics (Model m, double b1, double b2) {
		this.edgeSetCardinality = 0;
		this.labelCounterMap = new HashMap<String, Integer>();
		this.labelAverageDegreeMap = new HashMap<String, Integer>();
		this.betaScoreMap = new HashMap<String, Double>();
		this.inDegreeMap = new HashMap<RDFNode, Integer>();
		this.outDegreeMap = new HashMap<RDFNode, Integer>();
		this.averageCombinedDegreeMap = new HashMap<String, Double>();
		this.fMeasureMap = new HashMap<String, Double>();
		this.graph = m;
		this.beta1 = b1;
		this.beta2 = b2;
	}

	public Statistics getModelStatistics() throws IllegalStateException {
		if(this.graph != null)
			return this.getModelStatistics(this.graph);
		else
			throw new IllegalStateException("Need to set an RDF graph before the calculation of its statistics");
	}

	/**Calculates the statistics of the Model (RDF graph) passed
	 * as parameter. 
	 * <p>These statistics includes:
	 * <ul>
	 * <li>|E|, cardinality of the edge set</li>
	 * <li>In degree of the nodes</li>
	 * <li>Out degree of the nodes</li>
	 * <li>Frequency of each label</li>
	 * <li>Average Combined Degree (AVD) of each label</li>
	 * <li>The beta-score of each label</li>
	 * </ul>
	 * </p>
	 * 
	 * @param graph The RDF graph where the method computes the statistics*/
	public Statistics getModelStatistics(Model graph) {
		
		System.out.println("start making statistics...");
		//a thread to take notice of the progression
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(new MonitorStatisticsThread(), 20, 30, TimeUnit.SECONDS);

		//iterate other the triples
		StmtIterator iterator = graph.listStatements();
		while(iterator.hasNext()) {
			//+1 for the number of edges
			this.edgeSetCardinality++;
			//take the label on this edge
			Statement t = iterator.next();
			Property l = t.getPredicate();
			String labelName = l.toString();

			//update the map label counter
			Integer value = this.labelCounterMap.get(labelName);
			if(value == null) {
				//first time we see this label
				this.labelCounterMap.put(labelName, 1);
			}
			else {
				//update the value
				this.labelCounterMap.put(labelName, value + 1);
			}

			//update the maps about the degrees
			Resource subject = t.getSubject();
			RDFNode object = t.getObject();
			
			//here we exploit the nature of the directed edge to compute
			//in degree and out degree
			
			//update out degree of the subject
			value = outDegreeMap.get(subject);
			if(value == null) {
				//first time we see this subject
				outDegreeMap.put(subject, 1);
			}
			else {
				//update the out degree
				outDegreeMap.put(subject, value + 1);
			}

			//update in degree of the object
			value = inDegreeMap.get(object);
			if(value == null) {
				//first time we see this object
				inDegreeMap.put(object, 1);
			}
			else {
				//update the in degree
				inDegreeMap.put(object, value + 1);
			}

			//count of the degree of the triple
//			int count = 0;
//			StmtIterator subjectIterator = subject.listProperties();
//			while (subjectIterator.hasNext()) {
//				subjectIterator.next();
//				count++;
//			}
//			
//			if(object instanceof Resource) {
//				StmtIterator objectIterator = subject.listProperties();
//				while (objectIterator.hasNext()) {
//					objectIterator.next();
//					count++;
//				}
//			}
//
//			value = this.labelAverageDegreeMap.get(labelName);
//			if(value == null) {
//				//first time we see this label
//				//the -2 is due to the fact that if the edge is (u, v), we don't count u and v as neighbours
//				this.labelAverageDegreeMap.put(labelName, count - 2);
//			}
//			else {
//				//update the count
//				value = value + count - 2;
//				this.labelAverageDegreeMap.put(labelName, value);
//			}
		}//end iteration over the triples
		System.out.println("all edges read, computing scores...");
		scheduler.shutdownNow();
		
		//compute the ACD(l)
		double maxACD = 0;
		for (String label : labelAverageDegreeMap.keySet()) {
			//for each predicate in the database
			double acd =  ((double)labelAverageDegreeMap.get(label)/(double)labelCounterMap.get(label));
			this.averageCombinedDegreeMap.put(label, acd);
			if(acd > maxACD)
				maxACD = acd;
		}

		//creation of the scores for the labels
		for (String label : labelCounterMap.keySet()) {//for each label
			//compute the score
			double firstScore = ((double)labelCounterMap.get(label) / (double)edgeSetCardinality);
			double secondScore = (double)this.averageCombinedDegreeMap.get(label)/(double)maxACD;
			double score = beta1*firstScore + beta2*secondScore;
			double fMeasure = 2.0/(double)(1/firstScore + 1/secondScore);

			this.betaScoreMap.put(label, score);
			this.fMeasureMap.put(label, fMeasure);
		}
		//already done
//		printTheStatistics(this);

		return this;
	}
	
	/**Method to print the statistics at the end of the first run. Then you can read
	 * them instead of computing them until the database stays the same.
	 * */
	public void printTheStatistics(Statistics stat) {
		System.out.println("printing the statistics...");
		Map<String, String> map = PropertiesUsefulMethods.getProperties();
		
		String labelCounterMapPath = map.get("clustering.support.directory") + "statistics.csv";
		Path out = Paths.get(labelCounterMapPath);
		
		try(BufferedWriter writer = Files.newBufferedWriter(out)) {
			//|E|
			writer.write("edge set cardinality," + stat.edgeSetCardinality);
			writer.newLine();
			writer.newLine();
			
			writer.write("label counter map");
			writer.newLine();
			for(Entry<String, Integer> entry : stat.labelCounterMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}
			writer.newLine();
			
			writer.write("label average degree map");
			writer.newLine();
			for(Entry<String, Integer> entry : stat.labelAverageDegreeMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue()); 
				writer.newLine();
			}
			writer.newLine();

			writer.write("average combined degree map");
			writer.newLine();
			for(Entry<String, Double> entry : stat.averageCombinedDegreeMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue()); 
				writer.newLine();
			}
			writer.newLine();
			
			writer.write("harmonic mean map");
			writer.newLine();
			for(Entry<String, Double> entry : stat.fMeasureMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}
			writer.newLine();
			
			writer.write("in degree map");
			writer.newLine();
			for(Entry<RDFNode, Integer> entry : stat.inDegreeMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}
			writer.newLine();
			
			writer.write("out degree map");
			writer.newLine();
			for(Entry<RDFNode, Integer> entry : stat.outDegreeMap.entrySet()) {
				writer.write(entry.getKey() + "," +entry.getValue());
				writer.newLine();
			}
			
			writer.close();
			System.out.println("done");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**Returns the top k elements of the connectivity list computed
	 * in the getStatistics method.
	 * 
	 * @param k the number of elements from the top to be taken.
	 * @param measure a String describing what measure to use to rank the labels. Allowed 
	 * parameters here are: beta (for the beta score) and f-measure (for the f-measure scoring).*/
	public List<String> getKConnectivityList(int k, String measure) {
		if(this.betaScoreMap == null && this.betaScoreMap.isEmpty()) {
			throw new IllegalArgumentException("There is no score for the labels");
		}

		int counter = 0;
		List<String> connectivityList = new ArrayList<String>();
		Map<String, Double> scoreMap = null;
		
		if(measure.equals("beta")) {
			this.betaScoreMap = UsefulMethods.sortByValue(this.betaScoreMap);
			scoreMap = this.betaScoreMap;
		}
		else if(measure.equals("harmonic-mean")) {
			this.fMeasureMap = UsefulMethods.sortByValue(this.fMeasureMap);
			scoreMap = this.fMeasureMap;
		}
		
		for( String label : scoreMap.keySet()) {
			//for each label (we have ordered them)
			connectivityList.add(label);
			counter++;
			if(counter >= k)//if we reach the limit k before, we stop
				break;
		}

		printConnectivityList(connectivityList);
		return connectivityList;
	}

	private void printConnectivityList(List<String> connectivityList) {
		Map<String, String> map = PropertiesUsefulMethods.getProperties();
		
		String labelCounterMapPath = map.get("clustering.support.directory") + "connectivityList.csv";
		Path out = Paths.get(labelCounterMapPath);
		
		try(BufferedWriter writer = Files.newBufferedWriter(out)) {
			for(String st : connectivityList) {
				writer.write(st + ",");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	//getters and setters
	public int getEdgeSetCardinality() {
		return edgeSetCardinality;
	}
	public void setEdgeSetCardinality(int edgeSetCardinality) {
		this.edgeSetCardinality = edgeSetCardinality;
	}
	public Map<String, Integer> getLabelCounterMap() {
		return labelCounterMap;
	}
	public void setLabelCounterMap(Map<String, Integer> labelCounterMap) {
		this.labelCounterMap = labelCounterMap;
	}
	public Map<String, Integer> getLabelAverageDegreeMap() {
		return labelAverageDegreeMap;
	}
	public void setLabelAverageDegreeMap(Map<String, Integer> labelAverageDegreeMap) {
		this.labelAverageDegreeMap = labelAverageDegreeMap;
	}

	public double getBeta1() {
		return beta1;
	}

	public void setBeta1(double beta1) {
		this.beta1 = beta1;
	}

	public double getBeta2() {
		return beta2;
	}

	public void setBeta2(double beta2) {
		this.beta2 = beta2;
	}

	public Map<String, Double> getBetaScoreMap() {
		return betaScoreMap;
	}

	public void setBetaScoreMap(Map<String, Double> betaScoreMap) {
		this.betaScoreMap = betaScoreMap;
	}

	public Model getGraph() {
		return graph;
	}

	public void setGraph(Model graph) {
		this.graph = graph;
	}

	public Map<RDFNode, Integer> getInDegreeMap() {
		return inDegreeMap;
	}

	public void setInDegreeMap(Map<RDFNode, Integer> inDegreeMap) {
		this.inDegreeMap = inDegreeMap;
	}

	public Map<RDFNode, Integer> getOutDegreeMap() {
		return outDegreeMap;
	}

	public void setOutDegreeMap(Map<RDFNode, Integer> outDegreeMap) {
		this.outDegreeMap = outDegreeMap;
	}
	
	private class MonitorStatisticsThread extends Thread {
		public void run() {
			System.out.println("Read " + Statistics.this.edgeSetCardinality + " edges");
		}
	}


	private void uploadStatisticsFromCSVFile(String file) {
		Path inputPath = Paths.get(file);

		try (BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)){
			String line = "";
			while((line = reader.readLine()) != null) {
				if(line.equals("edge set cardinality")) {
					String cardinality = reader.readLine();
					int card = Integer.parseInt(cardinality);
					this.edgeSetCardinality = card;
				}
				else if(line.equals("in degree map")) {
					while(!(line = reader.readLine()).equals("\n")) {
						String[] parts = line.split(",");
						//...
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
