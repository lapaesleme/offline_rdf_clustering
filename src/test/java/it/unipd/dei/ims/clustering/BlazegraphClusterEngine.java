package it.unipd.dei.ims.clustering;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openrdf.model.Graph;
import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;

import it.unipd.dei.ims.clustering.utilities.UsefulMethods;
import it.unipd.dei.ims.statistics.BlazegraphStatistics;
import it.unipd.dei.ims.statistics.Statistics;
import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;

@Deprecated
public class BlazegraphClusterEngine {

	/**Repository representing where the RDF graph is stored. Threat carefully.
	 * */
	Repository repository;

	private String lang;
	private String outputPathDirectory ;

	/**Set of the source nodes (high out degree)*/
	private Set<String> sourceSet;
	/**Set of the terminal nodes (high in degree)*/
	private Set<String> terminalSet;
	/**Map to keep track of the color of the nodes*/
	private Map<String, Color> nodeColorMap;
	/**This Map keeps track of how many times a node has been visited*/
	private Map<String, Integer> nodeVisitMap;

	/**threshold values to decide if a node belongs to set T and/or set S or neither*/
	private int lambda_in=0, lambda_out=0;

	public enum Color {
		white,
		gray,
		black
	}

	/**keeps track of how many cluster have been printed*/
	private int counter = 1; 
	private int directoryCounter = 1;

	private static int checkCount = 0;

	public BlazegraphClusterEngine (Repository r) {
		this.repository = r;
		sourceSet = new HashSet<String>();
		terminalSet = new HashSet<String>();
		nodeColorMap = new HashMap<String, Color>();
		this.nodeVisitMap = new HashMap<String, Integer>();
	}

	public BlazegraphClusterEngine (Repository r, int lin, int lout) {
		this.repository = r;
		sourceSet = new HashSet<String>();
		terminalSet = new HashSet<String>();
		nodeColorMap = new HashMap<String, Color>();
		this.nodeVisitMap = new HashMap<String, Integer>();
		this.lambda_in = lin;
		this.lambda_out = lout;
	}

	/**@param r the path to the repository where the databas resides.
	 * @param lin lambda in.
	 * @param lout lambda out.
	 * */
	public BlazegraphClusterEngine (String r, int lin, int lout) {
		this.repository = BlazegraphUsefulMethods.getRepositoryFromPath(r);
		sourceSet = new HashSet<String>();
		terminalSet = new HashSet<String>();
		nodeColorMap = new HashMap<String, Color>();
		this.nodeVisitMap = new HashMap<String, Integer>();
		this.lambda_in = lin;
		this.lambda_out = lout;
	}

	public BlazegraphClusterEngine (Repository r, Statistics stat, int lin, int lout) {
		this.repository = r;
		sourceSet = new HashSet<String>();
		terminalSet = new HashSet<String>();
		nodeColorMap = new HashMap<String, Color>(2*stat.getEdgeSetCardinality());
		this.nodeVisitMap = new HashMap<String, Integer>(2*stat.getEdgeSetCardinality());
		this.lambda_in = lin;
		this.lambda_out = lout;
	}

	/**Thread to keep track of the execution of the algorithm.
	 * */
	private class NodesMonitorThread extends Thread {
		public void run() {
			System.out.println("Colored " + checkCount + " nodes");
		}
	}

	/**!!! This method must be called before starting the clustering algorithm !!!  
	 * Computes the sets T (of terminal nodes) and S (of source nodes) 
	 *  on the model of the engine.*/
	public void computeTerminalAndSourceNodesSets (BlazegraphStatistics stat) {
		System.out.println("finding the central nodes...");

		//get the language and where to write
		Map<String, String> propertyMap = UsefulMethods.getProperties();
		this.lang = propertyMap.get("model.writing.lang");
		this.outputPathDirectory = propertyMap.get("clusters.directory");


		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(new NodesMonitorThread(), 5, 30, TimeUnit.SECONDS);

		//color the nodes of white
		Map<String, Integer> inMap = stat.getInDegreeMap();

		//select the terminal nodes
		for(Entry<String, Integer> entry : inMap.entrySet()) {
			checkCount++;
			//these nodes are nodes with in degree >1. They are objects in this context
			String obj = entry.getKey();
			Color value = this.nodeColorMap.get(obj);
			if(value == null) {
				//first time we see this node
				nodeColorMap.put(obj, Color.white);
			}

			//in case, add to the terminal set
			Integer inDegree = entry.getValue();
			boolean ok;
			if(inDegree != null && inDegree >= lambda_in) {
				ok = terminalSet.add(obj);
			}
		}

		Map<String, Integer> outMap = stat.getOutDegreeMap();
		for(Entry<String, Integer> entry : outMap.entrySet()) {
			checkCount++;
			String subj = entry.getKey();
			Color value = this.nodeColorMap.get(subj);
			if(value == null) {
				//first time we see this node, color it white
				nodeColorMap.put(subj, Color.white);
			}

			//in case, add to the source set 
			Integer outDegree = entry.getValue();
			boolean ok;
			if(outDegree != null && outDegree >= lambda_out) {
				ok = sourceSet.add(subj);
			}

		}

		scheduler.shutdownNow();


		/*NB: reference for the future: a possibility is that the empty string is 
		 * used as RDF node to represent absence of information. THis is a strange
		 * choice, and the Set implementation in Java, when we try to introduce an element
		 * such as a Node which corresponds to an empty string,
		 * refuses to accept it. 
		 * 
		 * Usually the empty string (not a blank node, a node with an empty string) 
		 * is used as object, but it's strange, is like saying: this information is missing.
		 * So such a triple shouldn't even be present in the database because we 
		 * are not interested in "non information".
		 * 
		 * For now the choice is to ignore such node, removing 
		 * it from the graph.*/

		System.out.println("central nodes: done");
	}

	/**Creates the clusters. It is necessary to execute this method
	 * after the setup performed in @link{terminalAndSourceSetBuilding}
	 * 
	 * @param tau the radius of the cloud
	 * @param destination path to the directory where to print the damn clusters
	 * @throws QueryEvaluationException 
	 * @throws RepositoryException 
	 * */
	public List<Model> clustering(int tau, List<String> connectivityList, String destination) throws RepositoryException, QueryEvaluationException 
	{

		//need to initialize the repository
		try {
			this.repository.initialize();
		} catch (RepositoryException e) {
			this.repository.shutDown();
			e.printStackTrace();
		}
		//iterator over the source nodes
		Iterator<String> sourceIterator = sourceSet.iterator();

		List<Model> clusterList = new ArrayList<Model>();
		boolean first = true;

		//open the connection to the graph
		RepositoryConnection connection = BlazegraphUsefulMethods.
				getRepositoryConnection(this.repository);
		
		//for each source node
		while(sourceIterator.hasNext()) {
			String source = sourceIterator.next();
			if(source.toString().equals(""))
				//ignore the empty node
				continue;

			//take the color of this node
			Color sourceColor = nodeColorMap.get(source);
			if(sourceColor != Color.black) {
				//keep rockin' (create a new cluster)
				Model cluster = this.extendCluster(connection, source, tau, connectivityList);
				this.printTheDamnCluster(cluster, first, destination);
				first = false;
			}
		}
		//first close the connection, then the other way around
		connection.close();
		//close the repository, the job is done
		this.repository.shutDown();
		
		return clusterList;
	}

	/**Expands 1 cluster following my algorithm.
	 *<p> 
	 *This is the third version of the clustering algorithm, born because of the following
	 *considerations:
	 *</p> 
	 *
	 *<p>
	 *The node which have great out-degree should be the starting points of the algorithm. They are called 
	 *<b>source nodes</b>. The nodes which have great in-degree are probably nodes with useful information
	 *for many clusters. They should be used but not further explored. They are called <b>terminal nodes</b>.
	 *a node can be both source and terminal.
	 *</p>
	 *
	 *<p>
	 *A node which is neither a source node nor a terminal node is called <p>accessory node</p>, and we
	 *assume it carries information which is of little relevance to the cluster, but that can be
	 *useful for some queries, so we include it.
	 *</p>
	 *
	 *<p>
	 *Note that a literal node not necessarily is the object of only one triple. It can be the object of various 
	 *triples. That means that the literals that are terminal nodes are very useful for the cluster. Moreover, 
	 *often a terminal node is an IRI which contains only an ID. To obtain useful information, it is necessary
	 *to include in the cluster other nodes around the terminal nodes that are literals and better describe its nature.
	 *</p>
	 *
	 *
	 * @param source the starting node to build the cloud
	 * @param tau the radius of the cloud
	 * @param connectivityList list of Strings representing the labels of the edges we will explore
	 * @throws RepositoryException 
	 * @throws QueryEvaluationException 
	 * */
	public Model extendCluster(RepositoryConnection connection, String source, int tau, List<String> connectivityList) throws RepositoryException, QueryEvaluationException {

		//create new graph which is a cluster
		Model cluster = new TreeModel();


		//queue to keep track of the nodes that has to be explore yet (the information kept
		//is the node and the radius tau that we still have)
		Queue<Pair<String, Integer>> extendingQueue = new LinkedList<Pair<String, Integer>>();
		extendingQueue.add(new MutablePair<String, Integer>(source, tau));

		while(! extendingQueue.isEmpty() ) {
			Pair<String, Integer> sPair = extendingQueue.remove();
			String s = sPair.getLeft();
			//mark the source node s as visited (thus the clustering algorithm will end)
			nodeColorMap.put(s, Color.black);

			//keep track of how far off we are from the origin (update the radius)
			int radius = sPair.getRight() - 1;

			cluster = addAccessoryCloud(connection, cluster, s);

			//query the graph about the nodes around the node
			RepositoryResult<Statement> iterator = BlazegraphUsefulMethods.listStatements(connection, s);

			//trying to expand the cluster to other source nodes
			while(iterator.hasNext()) {
				Statement l = iterator.next();
				Value u = l.getObject();
				Color uColor = nodeColorMap.get(u.toString());

				if((radius > 0) && (uColor != Color.black) 
						&& connectivityList.contains(l.getPredicate().toString())) {
					//if the edge is explorable, the radius is till greater than 0 and the node u has not already been
					//used as central node
					if(sourceSet.contains(u.toString()) && !terminalSet.contains(u.toString())) {
						//if it is a source node and not a terminal node
						
						//enqueue the node and update its color
						Pair<String, Integer> uPair = new MutablePair<String, Integer>(u.toString(), radius);
						extendingQueue.add(uPair);
						nodeColorMap.put(u.toString(), Color.black);
						//add to the cluster
						cluster.add(l);
					}
				}
			}
		}
		return cluster;
	}

	/**Adds to the cluster represented by the argument Model the cloud
	 * of accessory nodes around the source node provided following the
	 * specifications of my algorithm.
	 * 
	 * @param cluster RDF model, the cluster to be populated with the cloud around the
	 * source node
	 * @param s the central node to be expanded with the accessory nodes (if they are present)
	 * @throws QueryEvaluationException 
	 * @throws RepositoryException 
	 * */
	public Model addAccessoryCloud(RepositoryConnection connection, Model cluster, String s) throws QueryEvaluationException, RepositoryException {

		//query the graph about the nodes around the subject
		RepositoryResult<Statement> iterator = BlazegraphUsefulMethods.listStatements(connection, s);
		//for each triple
		while(iterator.hasNext()) {
			Statement triple = iterator.next();
			org.openrdf.model.Value obj = triple.getObject();

			//if it is a simple accessory node
			if(!sourceSet.contains(obj.toString()) && !terminalSet.contains(obj.toString())) {
				//you can add the statement to the cluster
				cluster.add(triple);
			}
			//if it is a terminal node, it is still useful (it can be a string)
			else if(terminalSet.contains(obj.toString())) {
				cluster.add(triple);
				//if it is a URI, can be subject of other things, some useful information
				if(obj instanceof URI) {
					RepositoryResult<Statement> objIterator = 
							BlazegraphUsefulMethods.listStatements(connection, s);
					while(objIterator.hasNext()) {
						Statement t = objIterator.next();
						Value v = t.getObject();
						//if v is an accessory node or a literal, we add it
						if(v instanceof Literal || 
								(!sourceSet.contains(v.toString()) && !terminalSet.contains(v.toString()))) {
							cluster.add(t);
						}
					}
					objIterator.close();
				}
			}
		}
		iterator.close();
		return cluster;
	}

	/**Prints the parameter cluster rdf graph in a file using the 
	 * language specified in the properties in the path also specified in the properties.
	 * */
	private void printTheDamnCluster(Model cluster, boolean first, String destination) {
		if(first) {
			//clean the directory
			try {
				FileUtils.cleanDirectory(new File(destination));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.err.println("unable to clean the directory");
			}
			//create the first sub-directory
			(new File(destination + directoryCounter)).mkdir();
		}

		if(this.counter%2048 == 0) {
			//create a new directory in order to ease the work to the file system
			System.out.println(counter + " clusters produced");
			directoryCounter++;
			(new File(destination + directoryCounter)).mkdir();
		}

		//write down the clusters
		File f = new File(destination + directoryCounter + "/cluster_" + counter + ".nt");
		try (OutputStream out = new FileOutputStream(f)){
			RDFWriter writer = Rio.createWriter(RDFFormat.TURTLE, out);
			writer.startRDF();
			for(Statement st: cluster) {
				writer.handleStatement(st);
			}
			writer.endRDF();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error in writing the cluster number "+ counter);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Close didn't worked for the output stream of " + counter);
		} catch (RDFHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		counter++;
	}

}
