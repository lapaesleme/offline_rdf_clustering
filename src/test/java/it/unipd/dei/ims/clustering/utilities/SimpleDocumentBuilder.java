package it.unipd.dei.ims.clustering.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.base.Stopwatch;



/**The main goal of this class is to transform a directory made of RDF graphs
 * in a text xml file representing the collection of those documents that will
 * be later indexed.
 * 
 * It 
 * @author Dennis D.
 * @year 2018
 * */
@Deprecated
public class SimpleDocumentBuilder {

	/**path of the directory containing sub-directories with the cluster files*/
	private String dirPath;
	/**Map containing the properties of this project*/
	private Map<String, String> propertyMap;


	private final static Charset ENCODING = StandardCharsets.UTF_8;
	private static final String SUBJECT = "subject";
	private static final String OBJECT = "object";
	private static final String PREDICATE = "predicate";

	public SimpleDocumentBuilder() {
		propertyMap = UsefulMethods.getProperties();
		dirPath = propertyMap.get("clusters.directory");
	}



	/** This methods considers the following structure in the file system:
	 * 
	 * <p>
	 * A principal directory containing sub-directories. Each sub-directory
	 * is named with a natural number in ascending order. Every subdirectory 
	 * must contain only files .nt (or other formats) reperesenting RDF graphs.
	 * </p>
	 * <p>
	 * This method translates all the documents in one of these sub-directories in a unique
	 * xml file that can later be indexed and analyzed by Terrier as a collection of 
	 * documents.
	 * </p>
	 * 
	 * <p>
	 * The path of the directory to be read is indicated in the properties files at the 
	 * key 'clusters.directory'.
	 * The directory where it writes the file is indicated in the property files at the key 
	 * 'clusters.turned.to.documents.directory'.
	 * */
	public void translateGraphsToXmlDocuments () {
		//the directory path is in the variable dirPath
		File mainDirectory = new File(dirPath);
		//take only the directories 
		File[] directories = mainDirectory.listFiles(File::isDirectory);
		int counter = 0;
		String writingDirectory = propertyMap.get("clusters.turned.to.documents.directory");

		for(File readingDirectory : directories) {
			//create our document made by the graphs in this directory
			String writingDocument = writingDirectory + (counter++) + ".xml";
			Path outputPath = Paths.get(writingDocument);

			try(BufferedWriter writer = Files.newBufferedWriter(outputPath, ENCODING);) {
				writer.write("<collection>");
				writer.newLine();

				//need a Map to store the words that are present as subjects, predicates or objects
				Map<String, String> wordMap = new HashMap<String, String>();
				wordMap.put("subject", "");
				wordMap.put("predicate", "");
				wordMap.put("object", "");

				//now take all the graph files in the reading directory
				File[] graphFiles = readingDirectory.listFiles();
				for(File graph : graphFiles) {
					//now we read a graph, line by line, and sign down the words it contains
					BufferedReader reader = Files.newBufferedReader(graph.toPath(), ENCODING);

					//get the number of the graph and save it in the map
					String clusterID = getIDFromFile(graph);
					wordMap.put("ID", clusterID);

					//read the cluster and write down the words
					String line = "";
					while((line = reader.readLine()) != null) {
						addOneLineToMap(line, wordMap);

					}
					//end of cluster, close the reader
					reader.close();
					//now, in the map we have all the words of the cluster. We can write in the file
					writeOneDocumentXML(wordMap, writer);
				}
				//read all the clusters in this directory, time to close
				writer.write("</collection>");
				writer.flush();
				//the try/catch take charge to close the writer
				//				writer.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	/**The string line is taken and divided in three parts: subject, object and predicate.
	 * These parts are then processed to extrapolate the words deemed as useful. They
	 * are added to the map in their rightful position (subject, predicate or object).
	 * 
	 * */
	private static void addOneLineToMap(String line, Map<String, String> wordMap) {
		//reguar expression. We take the url within <...> but not the <>
		//or the literals
		String patternString = "<(.*?)>|\"(.*)\"(\\^\\^<(.*?)>)?|([0-9]+)$";

		//compile the pattern to match the strings and store them in the matcher
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(line);

		//counting if we are at the predicate, the subject or the object
		int counter = 0;

		while(matcher.find()) {
			//for each token found
			String work = matcher.group();

			if(work.equals(""))//just in case if the got epsilon (empty string)
				continue;
			else if(work.charAt(0) == '<') {//it's a url
				//take it without parenthesis 
				work = matcher.group(1);
				//work with the url
				work = takeFinalWordFromIRI(work);

				//get rid of possible not acceptable char in xml
				work = work.replaceAll("\\&", "&amp;");
				work = work.replaceAll("<", "&lt;");
				work = work.replaceAll(">", "&gt;");
				work = work.replaceAll("'", "&apos;");
				work = work.replaceAll("\"", "&quot;");


				if(counter==0) {
					//we are at the subject
					//add the string to the words we already have
					work = wordMap.get(SUBJECT) + " " + work.trim();
					wordMap.put(SUBJECT, work.trim());
					counter++;
				}
				else if(counter==1) {
					//we are at the predicate
					work = wordMap.get(PREDICATE) + " " + work.trim();
					wordMap.put(PREDICATE, work.trim());
					counter++;
				}
				else if(counter==2) {
					//we are at the object
					work = wordMap.get(OBJECT) + " " + work.trim();
					wordMap.put(OBJECT, work.trim());
					counter++;
				}
			}
			else if(work.charAt(0) == '"') {
				//literal, it should be an object
				work = matcher.group(2);
				work = work.replaceAll("\\&", "&amp;");
				work = work.replaceAll("<", "&lt;");
				work = work.replaceAll(">", "&gt;");

				//add to the words we already have
				work = wordMap.get(OBJECT) + " " + work.trim();
				wordMap.put(OBJECT, work.trim());
			}

		}
	}

	/**Given an URI/IRI, it takes the last word(s) and returns it.
	 * 
	 * */
	private static String takeFinalWordFromIRI(String elaborandum) {

		try {
			URL urlString = new URL(elaborandum);
			String elaboratum = urlString.getPath();

			//potrebbe anche esserci una reference alla fine dell'URL. 
			//è quella che ci interessa
			String ref = urlString.getRef();
			if(ref != null)
				elaboratum = ref;

			String[] splitStrings = elaboratum.split("/");//prima prendiamo l'ultima parte del path

			//questa potrebbe essere composta da più parole separate da '_', che si dividono
			String[] secondSplit = splitStrings[splitStrings.length-1].split("_");

			//si mettono i caratteri in un'unica stringa, separati da spazi
			String returnandum = "";
			for(String s : secondSplit) {
				returnandum = returnandum + " " + s;
			}

			return returnandum;

		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.err.println("error in takeFinalWordFromIRI, something went wrong in the elaboation of this string that should be an URL: " + elaborandum);
			return "";
		}
	}

	/**Given a map of words divided in subjects, predicates and objects, it writes them in an xml document.
	 * */
	private static void writeOneDocumentXML(Map<String,String> map, BufferedWriter writer) throws IOException {

		//si scrive il documento xml
		writer.write("\t<document>");
		writer.newLine();

		//id del documento
		writer.write("\t\t<docno>");
		writer.write(map.get("ID"));
		writer.write("</docno>");
		writer.newLine();

		//soggetto
		writer.write("\t\t<subject>" + map.get(SUBJECT) + "</subject>");
		writer.newLine();

		//predicato
		writer.write("\t\t<predicate>" + map.get(PREDICATE) + "</predicate>");
		writer.newLine();

		//soggetto
		writer.write("\t\t<object>" + map.get(OBJECT) + "</object>");
		writer.newLine();


		writer.write("\t</document>");

		writer.newLine();
		writer.flush();

	}


	/**Given a File path in the form <absolute_path>/name_of_the_file.something,
	 * returns a number if name_of_the_file contains a number (the first one found in the string).
	 * null otherwise.
	 * 
	 * @param path a File representing the path to be analyzed.
	 * */
	private static String getIDFromFile(File path) {
		String graphPath = path.toString();

		return getIDFromFileString(graphPath);
	}
	
	private static String getIDFromFileString(String path) {
		String[] pathParts = path.split("/");
		String fileName = pathParts[pathParts.length - 1];
		String regex = "[0-9]+";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(fileName);
		String clusterID = "";
		if(matcher.find())
			clusterID = matcher.group(0);

		return clusterID;
	}

	/**Only translates the RDF clusters in xml documents*/
	public static void main(String args[]) {
		Stopwatch timer = Stopwatch.createStarted();
		System.out.println("Starting writing the documents");
		SimpleDocumentBuilder docBuilder = new SimpleDocumentBuilder();
		docBuilder.translateGraphsToExtendedXmlDocuments();
		System.out.println("Documents written in " + timer.stop());
	}

	/**This methods works exactly as {@link translateGraphsToXmlDocuments} but it writes xml 
	 * documents that contains the structure of the graphs triple by triple
	 * @see translateGraphsToXmlDocuments*/
	public void translateGraphsToExtendedXmlDocuments () {
		//the main directory where there are the other directories containing clusters
		File mainDirectory = new File(dirPath);
		//take only the directories 
		File[] directories = mainDirectory.listFiles(File::isDirectory);
		
		//I want the directories ordered
		List<String> directoriesList = orderTheListOfPathByIntegerIdentificator(directories);
		
		int counter = 0;
		//the directory where we are going to write our xml files
		String writingDirectory = propertyMap.get("clusters.turned.to.documents.directory");

		//clean the directory
		try {
			FileUtils.cleanDirectory(new File(writingDirectory));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}
		
		for(String dir : directoriesList) {
			File readingDirectory = new File(dir);
			dealWithOneDirectory(readingDirectory, writingDirectory, ++counter);
			System.out.println("directory "+ counter +" finished");
		}
		
	}
	
	/**Given a list of paths that are characterized from one identificator appearing inside of them,
	 * this method returns a list that is the ordered list of those path by their ID.
	 * 
	 * */
	private static List<String> orderTheListOfPathByIntegerIdentificator(File[] list) {
		List<String> orderedList = new ArrayList<String>();
		for(File element : list) {
			orderedList.add(element.getAbsolutePath());
		}
		
		IdStringComparator comp = new SimpleDocumentBuilder().new IdStringComparator();
		Collections.sort(orderedList, comp);
		return orderedList;
	}
	
	/**My personal comparator to sort strings as I like them to be sorted
	 * (nice and clean, that is).
	 * The lexicographic order is not what I want in this case. 
	 * 
	 * 
	 * */
	class IdStringComparator implements Comparator<String>{
		public int compare(String string1, String string2) {
			String id1 = SimpleDocumentBuilder.getIDFromFileString(string1);
			Integer ID1 = Integer.parseInt(id1);
			String id2 = SimpleDocumentBuilder.getIDFromFileString(string2);
			Integer ID2 = Integer.parseInt(id2);
			
			return ID1 - ID2;
		}
		
	}

	/**Converts all the clusters in a directory in the corresponding xml document.
	 * 
	 * @param readingDirectory the current directory from where we are reading clusters.
	 * @param writingDirectory the path of the directory where we are writing the xml file
	 * @param docId Name of the xml file
	 * 
	 * */
	public static void dealWithOneDirectory(File readingDirectory, String writingDirectory, int docId) {
		//create a new xml document corresponding to this directory of clusters
		String writingDocument = writingDirectory + docId + ".xml";
		Path outputPath = Paths.get(writingDocument);

		try(BufferedWriter writer = Files.newBufferedWriter(outputPath, ENCODING);) {
			writer.write("<collection>");
			writer.newLine();

			//take all the clusters in this directory
			File[] graphFiles = readingDirectory.listFiles();
			List<String> graphFileList  = orderTheListOfPathByIntegerIdentificator(graphFiles); 
			for(String g : graphFileList) {
				dealWithOneCluster(new File(g), writer);
			}
//			for(File graph : graphFiles) {
//				dealWithOneCluster(graph, writer);
//			}

			writer.write("</collection>");
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**Converts one cluster in xml text 
	 * 
	 * @param cluster the graph file to be transformed
	 * @param writer The BufferedWriter to be used when writing*/
	public static void dealWithOneCluster(File cluster, BufferedWriter writer) {
		try(BufferedReader reader = Files.newBufferedReader(cluster.toPath(), ENCODING);){
			//get the id of the cluster
			String clusterID = getIDFromFile(cluster);
			writer.write("\t<document>");
			writer.newLine();
			writer.write("\t\t<docno>"+clusterID+"</docno>");
			writer.newLine();

			String triple = "";
			while((triple = reader.readLine()) != null) {
				writer.write("\t\t<triple>");
				writer.newLine();
				dealWithOneLine(triple, writer);
				writer.write("\t\t</triple>");
				writer.newLine();
			}
			writer.write("\t</document>");
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**Writes one line in xml format.
	 * 
	 * @throws IOException If there is some problem with the writer
	 * 
	 * */
	public static void dealWithOneLine(String line, BufferedWriter writer) throws IOException {
		//regular expression. We take the url within <...> but not the <>
		//or the literals
		String patternString = "<(.*?)>|\"(.*)\"(\\^\\^<(.*?)>)?|([0-9]+)$";

		//compile the pattern to match the strings and store them in the matcher
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(line);

		//counting if we are at the predicate, the subject or the object
		int counter = 0;

		while(matcher.find()) {
			//for each token found
			String work = matcher.group();

			if(work.equals(""))//just in case if the got epsilon (empty string)
				continue;
			else if(work.charAt(0) == '<') {//it's a url
				//take it without parenthesis 
				work = matcher.group(1);
				//work with the url
				work = takeFinalWordFromIRI(work);

				//get rid of possible not acceptable char in xml
				work = work.replaceAll("\\&", "&amp;");
				work = work.replaceAll("<", "&lt;");
				work = work.replaceAll(">", "&gt;");
				work = work.replaceAll("'", "&apos;");
				work = work.replaceAll("\"", "&quot;");


				if(counter==0) {
					//we are at the subject
					writer.write("\t\t\t<subject>" + work.trim() + "</subject>");
					writer.newLine();
					counter++;
				}
				else if(counter==1) {
					//we are at the predicate
					writer.write("\t\t\t<predicate>" + work.trim() + "</predicate>");
					writer.newLine();
					counter++;
				}
				else if(counter==2) {
					//we are at the object
					writer.write("\t\t\t<object>" + work.trim() + "</object>");
					writer.newLine();
					counter++;
				}
			}
			else if(work.charAt(0) == '"') {
				//literal, it should be an object
				work = matcher.group(2);
				work = work.replaceAll("\\&", "&amp;");
				work = work.replaceAll("<", "&lt;");
				work = work.replaceAll(">", "&gt;");

				writer.write("\t\t\t<object>" + work.trim() + "</object>");
				writer.newLine();
			}

		}
	}

}
