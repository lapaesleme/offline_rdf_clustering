package it.unipd.dei.ims.clustering.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.terrier.utilities.PathUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.StringUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.UrlUtilities;
import it.unipd.dei.ims.terrier.utilities.UsefulConstants;

/** This class transforms a bunch of documents containing RDF graphs in some format into 
 * xml documents.
 * <p>
 *  If you are having troubles with the size of the graphs, refer to http://archive.rdf4j.org/users/ch09.html
 *  
 *  The form of the documents:
 * 
 * <p>
 * <collection>
 * <document>
 * <docno>...</docno>
 * <triple>
 * <subject>...</subject>
 * <predicate>...</predicate>
 * <object>...</object>
 * </triple>
 * ...
 * </document>
 * ...
 * </collection>
 * */
public class DocumentBuilder {

	/** Path of the directory containing the files.*/
	private String mainDirectoryPath;
	
	/** Path of the directory where to put the xml files*/
	private String outputDirectory;
	
	public DocumentBuilder (String main, String out) {
		mainDirectoryPath = main;
		outputDirectory = out;
	}
	
	public void translateGraphsToXmlDocuments () throws MalformedURLException {
		//clean the directory where we are going to write the clusters
		try {
			FileUtils.cleanDirectory(new File(this.outputDirectory));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}

		//take all the files in the main directory (even in the sub-directories)
		Map<String, String> fileMap = PathUsefulMethods.getPathsAndIDsOfAllFilesInsideDirectoryRecursively(mainDirectoryPath);
		
		//XXX DEBUG
//		Map<String, String> fileMap = new HashMap<String, String>();
//		fileMap.put("2351600", "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/clusters/clusters/1149/cluster_2351609.ttl");
		//counts the number of documents written in an xml file
		int documentsCounter = 0; 
		int fileCounter = 1;
		
		BufferedWriter writer = null;
		//this map contains couple (id of the graph, collection of triples of the gaph)
		Map<String, Collection<Statement>> statementsMap = new HashMap<String, Collection<Statement>>();
		
		for(Entry<String, String> entry: fileMap.entrySet()) {
			//for every file
			String id = entry.getKey();
			String path = entry.getValue();
			
			try {
				//open the input stream to the file
				InputStream inputStream = new FileInputStream(new File(path));
				//prepare a collector to contain the triples
				StatementCollector collector = new StatementCollector();
				//read the file
				//TODO supposing it is in turtle syntax
				RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
				//link the collector to the parser
				rdfParser.setRDFHandler(collector);
				//parse the file
				rdfParser.parse(inputStream, "");
				
				//now get the statements composing the graph
				Collection<Statement> statements = collector.getStatements();
				
				//add the statements to a map
				statementsMap.put(id, statements);
				
				documentsCounter++;
				inputStream.close();
				if(documentsCounter >= 4096) {
					documentsCounter=0;
					//write all the graphs into a file
					this.printAFile(statementsMap, fileCounter++);
					
					statementsMap.clear();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (RDFParseException e) {
				e.printStackTrace();
			} catch (RDFHandlerException e) {
				// // handle a problem encountered by the RDFHandler
				e.printStackTrace();
			}
			
		}
		if(documentsCounter > 0) {
			documentsCounter=0;
			//write all the graphs ito a file
			this.printAFile(statementsMap, fileCounter++);
			
			statementsMap.clear();
		}
	}
	
	/**Prints all the clusters inside the map into a xml file*/
	private void printAFile(Map<String, Collection<Statement>> statementsMap, int fileCounter) {
		//name of the output file
		String outputFile = outputDirectory + fileCounter + ".xml";
		Path outputPath = Paths.get(outputFile);
		
		try(BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);) {
			writer.write("<collection>");
			writer.newLine();
			//write the documents
			this.writeDocuments(writer, statementsMap);
			
			
			writer.write("</collection>");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void writeDocuments(BufferedWriter writer, Map<String, Collection<Statement>> statementsMap) throws IOException {
		for(Entry<String, Collection<Statement>> entry: statementsMap.entrySet()) {
			//for every graph
			writer.write("\t<document>");
			writer.newLine();
			
			writer.write("\t\t<docno>" + entry.getKey() + "</docno>");
			writer.newLine();
			this.writeADocument(writer, entry.getValue());
			
			writer.write("\t</document>");
			writer.newLine();
		}
		writer.flush();
	}
	
	private void writeADocument(BufferedWriter writer, Collection<Statement> graph) throws IOException {
		for(Statement statement : graph) {
			//for each triple
			writer.write("\t\t<triple>");
			writer.newLine();
			
			this.printOneTriple(writer, statement);

			writer.write("\t\t</triple>");
			writer.newLine();
		}
	}
	
	private void printOneTriple(BufferedWriter writer, Statement t) throws IOException {
		Value subject = t.getSubject();
		Value predicate = t.getPredicate();
		Value object = t.getObject();
		
		String subjectString = UrlUtilities.takeFinalWordFromIRI(subject.toString());
		subjectString = StringUsefulMethods.checkCharacterInStringForXML(subjectString);
		writer.write("\t\t\t<subject>" + subjectString +"</subject>");
		writer.newLine();
		
		String predicateString = UrlUtilities.takeFinalWordFromIRI(predicate.toString());
		predicateString = StringUsefulMethods.checkCharacterInStringForXML(predicateString);
		writer.write("\t\t\t<predicate>" + predicateString + "</predicate>");
		writer.newLine();
		
		if(object instanceof Literal) {
			writer.write("\t\t\t<object>" + object.stringValue() + "</object>");
		}
		else {
			String objectString = UrlUtilities.takeFinalWordFromIRI(object.toString());
			objectString = StringUsefulMethods.checkCharacterInStringForXML(objectString);
			writer.write("\t\t\t<object>" + objectString.trim() + "</object>");
		}
		writer.newLine();
	}
	
	
	public static void main(String args[]) throws MalformedURLException {
		Map<String, String> propertyMap = UsefulMethods.getProperties();
		String mainDirectory = propertyMap.get("clusters.directory");
		String outputDirectory = propertyMap.get("clusters.turned.to.documents.directory");;
		
		Stopwatch timer = Stopwatch.createStarted();
		System.out.println("Starting writing the documents");
		DocumentBuilder docBuilder = new DocumentBuilder(mainDirectory, outputDirectory);
		
		docBuilder.translateGraphsToXmlDocuments();
		
		System.out.println("Documents written in " + timer.stop());
	}

}
