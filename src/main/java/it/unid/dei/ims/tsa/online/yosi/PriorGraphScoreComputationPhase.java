package it.unid.dei.ims.tsa.online.yosi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.openrdf.model.Model;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.yosi.datastructures.YosiGraph;

/**Phase 10.1 of the Yosi scoring algorithm.
 * This phase computes the prior score for a group of graphs.
 * This class needed to be created after the 
 * necessity of a way to score graphs that were retrieved
 * from disc as RDF files and not during the whole
 * computation of the Yosi algorithm. 
 * 
 * */

public class PriorGraphScoreComputationPhase {
	/** Path of the directory where to take the graphs
	 * */
	private String graphToScoreDirectoryPath;
	
	/** Path of the file where to write the graphs.
	 * */
	private String priorScoresOutputFilePath;
	
	private int totalDegree;
	
	/** Property map*/
	private Map<String, String> map;
	
	public PriorGraphScoreComputationPhase() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.graphToScoreDirectoryPath = map.get("graph.to.score.directory.path");
		this.priorScoresOutputFilePath = map.get("prior.scores.output.file.path");
	}
	
	public void computePriorForTheGraphs() throws RDFParseException, RDFHandlerException, IOException {
		//get the list of RDF files inside the directory
		File f = new File(this.graphToScoreDirectoryPath);
		File[] files = f.listFiles();
		
		//make sure the directory exists
		File f1 = new File(this.priorScoresOutputFilePath).getParentFile();
		if(!f1.exists()) {
			f1.mkdirs();
		}
		//open the writer
		Path o = Paths.get(this.priorScoresOutputFilePath);
		BufferedWriter writer = Files.newBufferedWriter(o, UsefulConstants.CHARSET_ENCODING);
		writer.write("<scores>\n");
		
		for(File graph : files) {
			if(graph.getName().equals(".DS_Store")) {
				continue;
			}
			String name = graph.getName();
			name = name.replaceAll("\\.ttl", "");
			
			Model g = new TreeModel(BlazegraphUsefulMethods.readOneGraph(graph.getAbsolutePath()));
			//now rank the graph g
			YosiGraph answerGraph = new YosiGraph(g);
			
			//SELECT SUM(degree_) from yosi_node
			double priorScore = answerGraph.computeQueryIndependentScore(this.totalDegree);
			writer.write("\t<score nodeId=\"" + name +"\">" + priorScore + "</score>");
			writer.newLine();
			writer.flush();
		}
		writer.write("</scores>");
		writer.close();
	}
	
	/** Test main*/
	public static void main(String[] args) {
		PriorGraphScoreComputationPhase execution = new PriorGraphScoreComputationPhase();
		try {
			execution.setTotalDegree(1146370);
			execution.computePriorForTheGraphs();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public String getGraphToScoreDirectoryPath() {
		return graphToScoreDirectoryPath;
	}

	public void setGraphToScoreDirectoryPath(String graphToScoreDirectoryPath) {
		this.graphToScoreDirectoryPath = graphToScoreDirectoryPath;
	}

	public String getPriorScoresOutputFilePath() {
		return priorScoresOutputFilePath;
	}

	public void setPriorScoresOutputFilePath(String priorScoresOutputFilePath) {
		this.priorScoresOutputFilePath = priorScoresOutputFilePath;
	}

	public int getTotalDegree() {
		return totalDegree;
	}

	public void setTotalDegree(int totalDegree) {
		this.totalDegree = totalDegree;
	}
}
